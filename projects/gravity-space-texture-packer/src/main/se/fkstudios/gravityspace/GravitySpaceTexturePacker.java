package se.fkstudios.gravityspace;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class GravitySpaceTexturePacker {

    public static void main(String[] args) {
        TexturePacker.Settings settings = new TexturePacker.Settings();

        // not all devices have support for bigger textures (without losing performance).
        // http://stackoverflow.com/questions/17292404/texture-packer-size-issue-in-libgdx
        settings.maxHeight = 1024;
        settings.maxWidth = 1024;
        settings.filterMag = Texture.TextureFilter.MipMapNearestLinear;
        settings.filterMin = Texture.TextureFilter.MipMapNearestLinear;

        TexturePacker.process(settings,
                "src/textures",
                "../gravity-space-game/core/assets",
                "textures");
    }
}
