package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.MenuButtonViewState

class ButtonForegroundRenderer(
        private val spriteBatch: SpriteBatch
) : ViewStateRenderer<MenuButtonViewState> {

    override fun render(projectionMatrix: Matrix4, viewState: MenuButtonViewState) {

        spriteBatch.projectionMatrix = projectionMatrix
        spriteBatch.begin()
        viewState.font.draw(
                spriteBatch,
                viewState.text,
                viewState.textX,
                viewState.textY,
                0f,
                -1,
                false
        )
        spriteBatch.end()
    }
}