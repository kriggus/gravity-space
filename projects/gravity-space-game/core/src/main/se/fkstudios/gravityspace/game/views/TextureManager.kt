package se.fkstudios.gravityspace.game.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion

class TextureManager {

    private val textureAtlas = TextureAtlas(Gdx.files.internal(TEXTURE_PACK_FILE_PATH))
    private val textureMap: MutableMap<String, Pair<Texture, TextureRegion>> = mutableMapOf()

    fun getAtlasTextureRegion(textureName: String): TextureRegion {
        return textureAtlas.findRegion(textureName)
    }

    fun getTextureRegion(textureName: String, fileType: String = "png"): TextureRegion {
        val textureAndRegion = textureMap[textureName]
        return when (textureAndRegion) {
            null -> {
                val texture = Texture(Gdx.files.internal("$textureName.$fileType"))
                val textureRegion = TextureRegion(texture)
                textureMap[textureName] = Pair(texture, textureRegion)
                textureRegion
            }
            else -> textureAndRegion.second
        }
    }

    fun dispose() {
        textureAtlas.dispose()
        textureMap.forEach { textureRegionPair ->
            textureRegionPair.value.first.dispose()
        }
    }

    companion object {
        private const val TEXTURE_PACK_FILE_PATH = "textures.atlas"
    }
}