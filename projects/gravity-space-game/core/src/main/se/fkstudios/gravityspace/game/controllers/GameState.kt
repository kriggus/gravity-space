package se.fkstudios.gravityspace.game.controllers

enum class GameState(val raw: Byte) {
    LOBBY(0),
    PLAYING(1),
    PAUSED(2);
}