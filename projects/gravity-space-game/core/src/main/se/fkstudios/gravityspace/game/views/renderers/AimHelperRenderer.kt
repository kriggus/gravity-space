package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.AimHelperViewState

class AimHelperRenderer(
        private val shapeRenderer: ShapeRenderer
) : ViewStateRenderer<AimHelperViewState> {

    override fun render(projectionMatrix: Matrix4, viewState: AimHelperViewState) {
        val mainLineStart = viewState.primaryLineStart()
        val mainLineEnd = viewState.primaryLineEnd()
        val leftLineStart = viewState.leftLineStart()
        val leftLineEnd = viewState.leftLineEnd()
        val rightLineStart = viewState.rightLineStart()
        val rightLineEnd = viewState.rightLineEnd()

        shapeRenderer.projectionMatrix = projectionMatrix
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)

        shapeRenderer.rectLine(
                leftLineStart.x,
                leftLineStart.y,
                leftLineEnd.x,
                leftLineEnd.y,
                viewState.thickness,
                viewState.secondaryStartColor,
                viewState.secondaryEndColor
        )

        shapeRenderer.rectLine(
                rightLineStart.x,
                rightLineStart.y,
                rightLineEnd.x,
                rightLineEnd.y,
                viewState.thickness,
                viewState.secondaryStartColor,
                viewState.secondaryEndColor
        )

        shapeRenderer.rectLine(
                mainLineStart.x,
                mainLineStart.y,
                mainLineEnd.x,
                mainLineEnd.y,
                viewState.thickness,
                viewState.primaryStartColor,
                viewState.primaryEndColor
        )

        shapeRenderer.end()
    }
}