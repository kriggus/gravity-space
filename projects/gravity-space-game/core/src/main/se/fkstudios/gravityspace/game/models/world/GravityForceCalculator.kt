package se.fkstudios.gravityspace.game.models.world

import se.fkstudios.gravityspace.common.utils.vectorLen
import se.fkstudios.gravityspace.common.utils.vectorNorX
import se.fkstudios.gravityspace.common.utils.vectorNorY
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class GravityForceCalculator {

    var distanceBetween: Float = 0f
        private set

    var forceAmountBetween: Float = 0f
        private set

    var forceBetweenX: Float = 0f
        private set

    var forceBetweenY: Float = 0f
        private set

    var target: WorldObject? = null
        private set

    var influencing: WorldObject? = null
        private set

    fun calculateForce(target: WorldObject, influencing: WorldObject, gravityConstant: Float, gravityMultiplier: Float) {
        this.target = target
        this.influencing = influencing

        val posDiffX = influencing.position.x - target.position.x
        val posDiffY = influencing.position.y - target.position.y

        distanceBetween = vectorLen(posDiffX, posDiffY)

        // To prevent singularities arising from zero distance or touching object from accelerate each other
        if (distanceBetween > (target.radius + influencing.radius + .1f)) {

            // Newton's law of gravity
            val forceAmountNumerator = gravityMultiplier * gravityConstant * target.mass * influencing.mass
            val forceAmountDenominator = distanceBetween * distanceBetween
            forceAmountBetween = forceAmountNumerator / forceAmountDenominator

            val norPosDiffX = vectorNorX(posDiffX, posDiffY)
            val norPosDiffY = vectorNorY(posDiffX, posDiffY)

            forceBetweenX = norPosDiffX * forceAmountBetween
            forceBetweenY = norPosDiffY * forceAmountBetween
        }
    }

    fun reverse() {
        forceBetweenX = -forceBetweenX
        forceBetweenY = -forceBetweenY
        val temp = influencing
        influencing = target
        target = temp
    }
}