package se.fkstudios.gravityspace.game.models.world.objects.profiles

import se.fkstudios.gravityspace.common.utils.vectorLen
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class ClosestAsMostMassiveProfile : GravityProfile {

    override fun dominating(
            target: WorldObject,
            mostMassive: WorldObject?,
            mostInfluencing: WorldObject?,
            closestDominator: WorldObject?
    ): WorldObject? = closestDominator

    override fun dominatingGravityMultiplier(
            target: WorldObject,
            dominating: WorldObject?,
            mostMassive: WorldObject?
    ): Float {
        if (dominating == null || !dominating.isDominator || mostMassive == null || dominating == mostMassive) {
            return 0f
        }

        val posDiffX = dominating.position.x - target.position.x
        val posDiffY = dominating.position.y - target.position.y

        val distBetween = vectorLen(posDiffX, posDiffY)
        val distBetweenPlusRadius = distBetween + (mostMassive.radius - dominating.radius)

        return (distBetween * distBetween * mostMassive.mass) /
                (distBetweenPlusRadius * distBetweenPlusRadius * dominating.mass)
    }
}