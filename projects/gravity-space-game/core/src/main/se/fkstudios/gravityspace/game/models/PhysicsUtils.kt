package se.fkstudios.gravityspace.game.models

import se.fkstudios.gravityspace.common.utils.vectorRotateX
import se.fkstudios.gravityspace.common.utils.vectorRotateY
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

fun density(mass: Float, radius: Float): Float =
        mass / area(radius)

fun mass(radius: Float, density: Float): Float =
        density * area(radius)

fun area(mass: Float, density: Float): Float =
        mass / density

fun area(radius: Float): Float =
        (Math.pow(radius.toDouble(), 2.0) * Math.PI).toFloat()

fun orbitingSpeed(
        gravityConstant: Float,
        gravityMultiplier: Float,
        distanceToPrimary: Float,
        massPrimary: Float
): Float {
    val speedSquared = gravityConstant * gravityMultiplier * massPrimary / distanceToPrimary
    return Math.sqrt(speedSquared.toDouble()).toFloat()
}

fun placeInOrbit(
        orbitingObject: WorldObject,
        primaryObject: WorldObject,
        distanceToPrimary: Float,
        angularOffset: Float,
        gravityConstant: Float,
        gravityMultiplier: Float,
        clockwise: Boolean
) {
    val displacementX = vectorRotateX(distanceToPrimary, 0f, angularOffset)
    val displacementY = vectorRotateY(distanceToPrimary, 0f, angularOffset)
    val positionX = primaryObject.position.x + displacementX
    val positionY = primaryObject.position.y + displacementY
    orbitingObject.setPosition(positionX, positionY)

    var orbitingSpeed = orbitingSpeed(gravityConstant, gravityMultiplier, distanceToPrimary, primaryObject.mass)
    if (clockwise) orbitingSpeed = -orbitingSpeed
    val velocityX = primaryObject.velocity.x + vectorRotateX(0f, orbitingSpeed, angularOffset)
    val velocityY = primaryObject.velocity.y + vectorRotateY(0f, orbitingSpeed, angularOffset)
    orbitingObject.setVelocity(velocityX, velocityY)
}