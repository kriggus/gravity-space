package se.fkstudios.gravityspace.game.views

import com.badlogic.gdx.Gdx

const val PIXELS_PER_UNIT = 100f

fun screenToWorld(scalar: Float): Float =
        scalar / PIXELS_PER_UNIT

fun worldToScreen(scalar: Float): Float =
        scalar * PIXELS_PER_UNIT

fun Float.toScreen(): Float =
        worldToScreen(this)

fun Float.toWorld(): Float =
        screenToWorld(this)

fun Float.invertX(): Float =
        Gdx.graphics.width - this

fun Float.invertY(): Float =
        Gdx.graphics.height - this