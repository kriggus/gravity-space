package se.fkstudios.gravityspace.game.models.players

import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship

class PlayerManager(
        private val playerMap: MutableMap<Int, Player> = mutableMapOf()
) {
    val players: Collection<Player>
        get() = playerMap.values

    fun addNewPlayer(clientId: Int, state: PlayerState): Player {
        val player = Player(clientId, smallestTeamNo(), state)
        playerMap[clientId] = player
        return player
    }

    private fun smallestTeamNo(): Int =
            if (teamSize(2) >= teamSize(1)) 1 else 2

    private fun teamSize(teamNo: Int): Int = playerMap.values.fold(0) { size, player ->
        size + if (player.teamNo == teamNo) 1 else 0
    }

    fun removePlayer(clientId: Int): Player? =
            playerMap.remove(clientId)

    fun setTeam(clientId: Int, teamNo: Int) {
        val player = findPlayer(clientId)
        player?.teamNo = teamNo
    }

    fun findPlayer(clientId: Int): Player? =
            playerMap[clientId]

    fun findPlayer(spaceship: Spaceship): Player? =
            playerMap.values.find { it.spaceship === spaceship }

    fun score(teamNo: Int): Int = playerMap.values.fold(0) { points, player ->
        points + playerScore(player, teamNo)
    }

    private fun playerScore(player: Player, teamNo: Int): Int = when (player.state) {
        PlayerState.PLAYING, PlayerState.PAUSED -> {
            val playerKills = if (player.teamNo == teamNo) player.kills else 0
            val playerDeaths = if (player.teamNo == teamNo) player.deaths else 0
            Math.max(0, playerKills * 2 - playerDeaths)
        }
        else -> 0
    }
}