package se.fkstudios.gravityspace.game.controllers

import com.badlogic.gdx.math.Rectangle
import se.fkstudios.gravityspace.game.views.viewstates.MenuButtonViewState
import se.fkstudios.gravityspace.game.views.viewstates.ViewStateManager

class Menu(
        viewportWidth: Int,
        viewportHeight: Int,
        viewStateManager: ViewStateManager
) {
    private var startHitArea: Rectangle = Rectangle()
    private var resumeHitArea: Rectangle = Rectangle()
    private var restartHitArea: Rectangle = Rectangle()

    fun inStartHitArea(x: Float, y: Float): Boolean =
            startHitArea.contains(x, y)

    fun inResumeHitArea(x: Float, y: Float): Boolean =
            resumeHitArea.contains(x, y)

    fun inRestartHitArea(x: Float, y: Float): Boolean =
            restartHitArea.contains(x, y)

    init {
        startHitArea.x = .2f * viewportWidth
        startHitArea.y = .4f * viewportHeight
        startHitArea.width = .6f * viewportWidth
        startHitArea.height = .2f * viewportHeight
        viewStateManager.addMenuButtonState(MenuButtonViewState(GameState.LOBBY, startHitArea, "START"))

        resumeHitArea.x = .2f * viewportWidth
        resumeHitArea.y = .55f * viewportHeight
        resumeHitArea.width = .6f * viewportWidth
        resumeHitArea.height = .2f * viewportHeight
        viewStateManager.addMenuButtonState(MenuButtonViewState(GameState.PAUSED, resumeHitArea, "RESUME"))

        restartHitArea.x = .2f * viewportWidth
        restartHitArea.y = .25f * viewportHeight
        restartHitArea.width = .6f * viewportWidth
        restartHitArea.height = .2f * viewportHeight
        viewStateManager.addMenuButtonState(MenuButtonViewState(GameState.PAUSED, restartHitArea, "RESTART"))
    }
}