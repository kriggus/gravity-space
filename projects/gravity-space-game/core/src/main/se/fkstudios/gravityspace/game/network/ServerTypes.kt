package se.fkstudios.gravityspace.game.network

import com.badlogic.gdx.net.Socket

typealias ClientConnectedHandler = (connection: ClientConnection) -> Unit
typealias ClientDisconnectedHandler = (connection: ClientConnection) -> Unit
typealias ServerReceiveHandler = (connection: ClientConnection, messageType: Int, payloadBuffer: ByteArray, payloadLength: Int) -> Unit
typealias ClientConnectionCreator = (id: Int, socket: Socket, onStop: (connection: ClientConnection) -> Unit) -> ClientConnection