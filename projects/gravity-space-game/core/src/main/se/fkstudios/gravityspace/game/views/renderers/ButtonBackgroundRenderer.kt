package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.MenuButtonViewState

class ButtonBackgroundRenderer(
        private val shapeRenderer: ShapeRenderer
) : ViewStateRenderer<MenuButtonViewState> {

    override fun render(projectionMatrix: Matrix4, viewState: MenuButtonViewState) {
        Gdx.gl.glEnable(GL30.GL_BLEND)
        Gdx.gl.glLineWidth(10f)

        shapeRenderer.projectionMatrix = projectionMatrix
        shapeRenderer.color = viewState.borderColor
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        shapeRenderer.rect(
                viewState.x,
                viewState.y,
                viewState.width,
                viewState.height
        )
        shapeRenderer.end()
        Gdx.gl.glLineWidth(1f)

        shapeRenderer.color = viewState.fillColor
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.rect(
                viewState.x,
                viewState.y,
                viewState.width,
                viewState.height
        )
        shapeRenderer.end()

        Gdx.gl.glDisable(GL30.GL_BLEND)
    }
}