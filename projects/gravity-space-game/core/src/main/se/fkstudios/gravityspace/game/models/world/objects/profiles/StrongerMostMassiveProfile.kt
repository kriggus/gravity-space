package se.fkstudios.gravityspace.game.models.world.objects.profiles

import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class StrongerMostMassiveProfile : GravityProfile {

    override fun dominating(
            target: WorldObject,
            mostMassive: WorldObject?,
            mostInfluencing: WorldObject?,
            closestDominator: WorldObject?
    ): WorldObject? = mostMassive

    override fun dominatingGravityMultiplier(
            target: WorldObject,
            dominating: WorldObject?,
            mostMassive: WorldObject?
    ): Float = when {
        dominating != null -> .5f
        else -> 0f
    }
}