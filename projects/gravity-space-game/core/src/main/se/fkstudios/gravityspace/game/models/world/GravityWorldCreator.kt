package se.fkstudios.gravityspace.game.models.world

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Array
import se.fkstudios.gravityspace.common.utils.*
import se.fkstudios.gravityspace.game.models.players.Player
import se.fkstudios.gravityspace.game.models.world.objects.Missile
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject
import se.fkstudios.gravityspace.game.models.world.objects.profiles.ClosestAsMostMassiveProfile
import se.fkstudios.gravityspace.game.models.world.objects.profiles.StrongerMostMassiveProfile
import se.fkstudios.gravityspace.game.views.TextureManager
import se.fkstudios.gravityspace.game.views.viewstates.*

class GravityWorldCreator(
        private val textureManager: TextureManager,
        private val viewStateManager: ViewStateManager
) {

    fun newSpaceshipForPlayer(world: GravityWorld, player: Player) {
        val spaceship = Spaceship(
                "${player.name}-spaceship",
                ClosestAsMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(1.33f),
                mass = 1f,
                maxThrust = 6f,
                maxSpeed = 12f,
                fuelCapacity = 100f
        )

        val textureName = when (player.teamNo) {
            1 -> "player-green-${MathUtils.random(1, 2)}"
            2 -> "player-orange-${MathUtils.random(1, 2)}"
            else -> throw Exception("Unsupported team")
        }

        val textureRegion = textureManager.getAtlasTextureRegion(textureName)
        val textureViewState = BoundTextureRegionViewState(textureRegion, spaceship, 2)
        viewStateManager.bindCameraState(spaceship, textureViewState)

        val thrustViewState = ThrustViewState(spaceship, 1)
        viewStateManager.bindCameraState(spaceship, thrustViewState)

        val aimHelperViewState = AimHelperViewState(spaceship, 5)
        viewStateManager.bindCameraState(spaceship, aimHelperViewState)

        val playerNameViewState = PlayerNameViewState(player, spaceship)
        viewStateManager.bindCameraState(spaceship, playerNameViewState)

        player.spaceship = spaceship
    }

    fun newMissile(world: GravityWorld, spaceship: Spaceship, teamNo: Int): Missile {
        val name = "${spaceship.name}-missile${MathUtils.random(0, 1000000)}"

        val startPosXOffset = vectorRotateX(0f, spaceship.height * 1.1f, spaceship.angleDegrees)
        val startPosYOffset = vectorRotateY(0f, spaceship.height * 1.1f, spaceship.angleDegrees)
        val startPosition = Vector2(spaceship.position.x + startPosXOffset, spaceship.position.y + startPosYOffset)

        val startVelXOffset = vectorRotateX(0f, 30f, spaceship.angleDegrees)
        val startVelYOffset = vectorRotateY(0f, 30f, spaceship.angleDegrees)
        val startVelocity = Vector2(spaceship.velocity.x + startVelXOffset, spaceship.velocity.y + startVelYOffset)

        val missile = Missile(
                name = name,
                gravityProfile = ClosestAsMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(.33f),
                mass = .01f,
                startPosition = startPosition,
                startVelocity = startVelocity,
                shooter = spaceship)

        missile.setVelocity(spaceship.velocity.x + startVelXOffset, spaceship.velocity.y + startVelYOffset)

        val textureName = when (teamNo) {
            1 -> "missile-green-1"
            2 -> "missile-orange-1"
            else -> throw Exception("Unsupported team")
        }

        val textureRegion = textureManager.getAtlasTextureRegion(textureName)
        val viewState = BoundTextureRegionViewState(textureRegion, missile, 1)
        viewStateManager.bindCameraState(missile, viewState)

        return missile
    }

    fun newWorld(
            onContact: (contact: Contact) -> Unit,
            onOutsideWorld: (worldObject: WorldObject) -> Unit,
            onDispose: (worldObject: WorldObject) -> Unit
    ): GravityWorld {
        val box2dWorld = World(Vector2(0f, 0f), true)
        val world = GravityWorld(box2dWorld, .0004f, 220f, onContact, onOutsideWorld, onDispose)

        val centerPlanet = newCenterPlanet(world)
        val planet1 = newPlanet1(world)
        val planet2 = newPlanet2(world)
        val planet3 = newPlanet3(world)
        val planetTeam1 = newPlanetTeam1(world)
        val planetTeam2 = newPlanetTeam2(world)
        val asteroids = newAsteroidsBelt(world, 50)

        val worldObjects = GdxArray<WorldObject>()
        worldObjects.add(centerPlanet)
        worldObjects.add(planet1)
        worldObjects.add(planet2)
        worldObjects.add(planet3)
        worldObjects.add(planetTeam1)
        worldObjects.add(planetTeam2)
        worldObjects.addAll(asteroids)

        world.initWorldObjects(worldObjects, centerPlanet, planetTeam1, planetTeam2)

        world.placeInOrbit(planet1, centerPlanet, 70f, 0f)
        world.placeInOrbit(planet2, centerPlanet, 70f, 120f)
        world.placeInOrbit(planet3, centerPlanet, 70f, 240f)
        world.placeInOrbit(planetTeam1, centerPlanet, 120f, 180f)
        world.placeInOrbit(planetTeam2, centerPlanet, 120f, 0f)

        for (i in 0 until asteroids.size) {
            val distanceToPlanet = 180f / 1.1f + 180f * 0.1f * MathUtils.random()
            val angularOffset = 360f * MathUtils.random()
            world.placeInOrbit(asteroids[i], centerPlanet, distanceToPlanet, angularOffset, true)
        }

        val backgroundTexture = textureManager.getTextureRegion("background-02")
        val backgroundViewState = UnboundTextureRegionViewState(
                textureRegion = TextureRegion(backgroundTexture),
                renderPriority = 0,
                width = Gdx.graphics.widthAsFloat,
                height = Gdx.graphics.heightAsFloat,
                position = Vector2(0f, 0f),
                origin = Vector2(0f, 0f)
        )
        viewStateManager.addScreenBackgroundState(backgroundViewState)

        return world
    }

    private fun newCenterPlanet(world: GravityWorld): WorldObject {
        val planet = WorldObject(
                name = "planet",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(24f),
                mass = 4000000f,
                startPosition = Vector2(0f, 0f),
                startAngularVelocity = -0.02f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-01")
        val viewState = BoundTextureRegionViewState(
                textureRegion = textureRegion,
                model = planet,
                renderPriority = 4
        )
        viewStateManager.bindCameraState(planet, viewState)

        return planet
    }

    private fun newPlanet1(world: GravityWorld): WorldObject {
        val moon = WorldObject(
                name = "planet1",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(12f),
                mass = 5000f,
                startAngle = 1.5f * MathUtils.PI2,
                startAngularVelocity = -0.015f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-blue-1")
        val viewState = BoundTextureRegionViewState(textureRegion, moon, 3)
        viewStateManager.bindCameraState(moon, viewState)

        return moon
    }

    private fun newPlanet2(world: GravityWorld): WorldObject {
        val moon = WorldObject(
                name = "planet2",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(10f),
                mass = 5000f,
                startAngle = 0.1f * MathUtils.PI2,
                startAngularVelocity = -0.02f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-blue-2")
        val viewState = BoundTextureRegionViewState(textureRegion, moon, 3)
        viewStateManager.bindCameraState(moon, viewState)

        return moon
    }

    private fun newPlanet3(world: GravityWorld): WorldObject {
        val moon = WorldObject(
                name = "planet3",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(8f),
                mass = 4000f,
                startAngle = 0.1f * MathUtils.PI2,
                startAngularVelocity = -0.01f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-blue-3")
        val viewState = BoundTextureRegionViewState(textureRegion, moon, 3)
        viewStateManager.bindCameraState(moon, viewState)

        return moon
    }

    private fun newPlanetTeam1(world: GravityWorld): WorldObject {
        val planet = WorldObject(
                name = "planetTeam1",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(4f),
                mass = 2500f,
                startAngle = 0.1f * MathUtils.PI2,
                startAngularVelocity = -0.009f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-green-1")
        val viewState = BoundTextureRegionViewState(textureRegion, planet, 3)
        viewStateManager.bindCameraState(planet, viewState)

        return planet
    }

    private fun newPlanetTeam2(world: GravityWorld): WorldObject {
        val planet = WorldObject(
                name = "planetTeam2",
                gravityProfile = StrongerMostMassiveProfile(),
                body = newDynamicBody(world.box2dWorld),
                shape = newCircleShape(4f),
                mass = 2200f,
                startAngle = 0.1f * MathUtils.PI2,
                startAngularVelocity = -0.02f * MathUtils.PI2,
                isDominator = true
        )

        val textureRegion = textureManager.getAtlasTextureRegion("planet-orange-1")
        val viewState = BoundTextureRegionViewState(textureRegion, planet, 3)
        viewStateManager.bindCameraState(planet, viewState)

        return planet
    }

    private fun newAsteroidsBelt(world: GravityWorld, count: Int): Array<WorldObject> {
        val snails = Array<WorldObject>()
        val baseRadius = 1f
        val baseMass = 1f
        for (snailNo: Int in 0 until count) {
            val sizeVariation = MathUtils.random()
            val asteroid = WorldObject(
                    name = "asteroids$snailNo",
                    gravityProfile = StrongerMostMassiveProfile(),
                    body = newDynamicBody(world.box2dWorld),
                    shape = newCircleShape(baseRadius / 2f + baseRadius * sizeVariation),
                    mass = baseMass / 2f + baseMass * sizeVariation,
                    startAngle = MathUtils.random(0f, 360f),
                    startAngularVelocity = -MathUtils.random() * MathUtils.PI
            )

            val textureName = when {
                sizeVariation > 5f / 6f -> "asteroid-1"
                sizeVariation > 4f / 6f -> "asteroid-2"
                sizeVariation > 3f / 6f -> "asteroid-3"
                sizeVariation > 2f / 6f -> "asteroid-4"
                sizeVariation > 1f / 6f -> "asteroid-5"
                else -> "asteroid-6"
            }

            val textureRegion = textureManager.getAtlasTextureRegion(textureName)
            val viewState = BoundTextureRegionViewState(
                    textureRegion = textureRegion,
                    model = asteroid,
                    renderPriority = 3
            )
            viewStateManager.bindCameraState(asteroid, viewState)
            snails.add(asteroid)
        }
        return snails
    }

    private fun newCircleShape(radius: Float): CircleShape =
            CircleShape().also { it.radius = radius }

    private fun newDynamicBody(box2dWorld: World): Body =
            box2dWorld.createBody(BodyDef().also { it.type = BodyDef.BodyType.DynamicBody })

}