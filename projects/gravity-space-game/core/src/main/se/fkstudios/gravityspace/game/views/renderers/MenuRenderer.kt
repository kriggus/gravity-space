package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.controllers.GameState
import se.fkstudios.gravityspace.game.views.viewstates.MenuButtonViewState

class MenuRenderer(
        private val spriteBatch: SpriteBatch,
        private val shapeRenderer: ShapeRenderer,
        private val buttonBackgroundRenderer: ButtonBackgroundRenderer = ButtonBackgroundRenderer(shapeRenderer),
        private val buttonForegroundRenderer: ButtonForegroundRenderer = ButtonForegroundRenderer(spriteBatch)
) {

    fun render(projectionMatrix: Matrix4, viewStates: List<MenuButtonViewState>, gameState: GameState) {
        renderButtonBackgrounds(viewStates, projectionMatrix, gameState)
        renderButtonForegrounds(viewStates, projectionMatrix, gameState)
    }

    private fun renderButtonForegrounds(viewStates: List<MenuButtonViewState>, projectionMatrix: Matrix4, gameState: GameState) {
        for (i in 0 until viewStates.size) {
            if (viewStates[i].activeGameState == gameState) {
                buttonForegroundRenderer.render(projectionMatrix, viewStates[i])
            }
        }
    }

    private fun renderButtonBackgrounds(viewStates: List<MenuButtonViewState>, projectionMatrix: Matrix4, gameState: GameState) {
        for (i in 0 until viewStates.size) {
            if (viewStates[i].activeGameState == gameState) {
                buttonBackgroundRenderer.render(projectionMatrix, viewStates[i])
            }
        }
    }
}