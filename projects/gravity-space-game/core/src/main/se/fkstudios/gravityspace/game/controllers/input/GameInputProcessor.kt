package se.fkstudios.gravityspace.game.controllers.input

import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import se.fkstudios.gravityspace.game.controllers.GameCamera

class GameInputProcessor(
        private val camera: GameCamera,
        private val onClick: (screenX: Float, screenY: Float) -> Unit
) : InputProcessor {

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = false

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        onClick(screenX.toFloat(), screenY.toFloat())
        return true
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = false

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean = false

    override fun scrolled(amount: Int): Boolean {
        val zoomChange = Math.exp(amount * SCROLL_ZOOM_MODIFIER).toFloat()
        when (amount > 0) {
            true -> camera.zoom += zoomChange
            false -> camera.zoom -= zoomChange
        }
        return true
    }

    override fun keyTyped(character: Char): Boolean = false

    override fun keyUp(keycode: Int): Boolean = false

    override fun keyDown(keycode: Int): Boolean = when (keycode) {
        Input.Keys.PLUS -> {
            camera.zoom += Math.exp(0.1f * SCROLL_ZOOM_MODIFIER).toFloat()
            true
        }
        Input.Keys.MINUS -> {
            camera.zoom -= Math.exp(0.1f * SCROLL_ZOOM_MODIFIER).toFloat()
            true
        }
        Input.Keys.R -> {
            camera.rotate(10f)
            true
        }
        Input.Keys.T -> {
            camera.rotate(-10f)
            true
        }
        else -> false
    }

    companion object {
        private const val SCROLL_ZOOM_MODIFIER = 0.1
    }
}

