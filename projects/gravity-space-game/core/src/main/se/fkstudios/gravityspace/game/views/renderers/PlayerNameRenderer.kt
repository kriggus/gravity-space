package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import se.fkstudios.gravityspace.game.controllers.GameCamera
import se.fkstudios.gravityspace.game.views.viewstates.PlayerNameViewState

class PlayerNameRenderer(
        private val spriteBatch: SpriteBatch
) {

    fun render(camera: GameCamera, viewState: PlayerNameViewState) {
        if (viewState.shouldRender) {
            spriteBatch.projectionMatrix = camera.combined
            spriteBatch.begin()

            val font = viewState.font
            font.data.scaleX = camera.zoom
            font.data.scaleY = camera.zoom
            font.draw(
                    spriteBatch,
                    viewState.text,
                    viewState.x,
                    viewState.y,
                    0f,
                    -1,
                    false
            )

            spriteBatch.end()
        }
    }
}