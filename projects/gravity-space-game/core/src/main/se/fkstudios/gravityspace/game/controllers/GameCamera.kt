package se.fkstudios.gravityspace.game.controllers

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.game.models.players.Player
import se.fkstudios.gravityspace.game.views.PIXELS_PER_UNIT

class GameCamera(
        viewportWidth: Float,
        viewportHeight: Float,
        startPositionX: Float = 0f,
        startPositionY: Float = 0f,
        private val minZoom: Float = 4f,
        private val maxZoom: Float = 256f
) : OrthographicCamera(viewportWidth, viewportHeight) {

    private val screenTargetPosition: Vector3 = Vector3(startPositionX, startPositionY, POSITION_Z)
    private val screenTargetMinPosition: Vector2 = Vector2()
    private val screenTargetMaxPosition: Vector2 = Vector2()

    init {
        near = NEAR
        far = FAR
        position.x = 0f
        position.y = 0f
        position.z = POSITION_Z
        screenTargetPosition.x = 0f
        screenTargetPosition.y = 0f
        screenTargetPosition.z = POSITION_Z
        zoom = minZoom + 10f
    }

    fun rotation(): Float =
            (Math.atan2(up.x.toDouble(), up.y.toDouble()) * MathUtils.radiansToDegrees).toFloat()

    fun update(timeDelta: Float, state: GameState, players: Collection<Player>) {
        if (state == GameState.PLAYING && players.isNotEmpty()) {
            updateTargetPosition(players)
            updatePosition(timeDelta)
            updateZoom(timeDelta)
        }
        super.update()
    }

    private fun updateTargetPosition(players: Collection<Player>) {
        screenTargetMinPosition.x = Float.POSITIVE_INFINITY
        screenTargetMinPosition.y = Float.POSITIVE_INFINITY

        screenTargetMaxPosition.x = Float.NEGATIVE_INFINITY
        screenTargetMaxPosition.y = Float.NEGATIVE_INFINITY

        screenTargetPosition.x = 0f
        screenTargetPosition.y = 0f

        var positionUpdateCount = 0
        for (player in players) {
            val spaceship = player.spaceship
            if (spaceship != null && player.state == PlayerState.PLAYING) {
                positionUpdateCount++

                val position = spaceship.position

                screenTargetPosition.x += position.x * PIXELS_PER_UNIT
                screenTargetPosition.y += position.y * PIXELS_PER_UNIT

                screenTargetMinPosition.x = Math.min(screenTargetMinPosition.x, position.x * PIXELS_PER_UNIT)
                screenTargetMinPosition.y = Math.min(screenTargetMinPosition.y, position.y * PIXELS_PER_UNIT)

                screenTargetMaxPosition.x = Math.max(screenTargetMaxPosition.x, position.x * PIXELS_PER_UNIT)
                screenTargetMaxPosition.y = Math.max(screenTargetMaxPosition.y, position.y * PIXELS_PER_UNIT)
            }
        }
        screenTargetPosition.scl(1f / Math.max(1, positionUpdateCount))
    }

    private fun updatePosition(timeDelta: Float) {
        position.lerp(screenTargetPosition, 2f * timeDelta)
        position.z = POSITION_Z
    }

    private fun updateZoom(timeDelta: Float) {
        val width = screenTargetMaxPosition.x - screenTargetMinPosition.x
        val height = screenTargetMaxPosition.y - screenTargetMinPosition.y

        val zoomX = width / viewportWidth
        val zoomY = height / viewportHeight

        val targetZoom = Math.max(zoomX, zoomY) + 10f
        if (targetZoom > minZoom && targetZoom < maxZoom) {
            zoom = MathUtils.lerp(zoom, targetZoom, 2f * timeDelta)
        }
    }

    companion object {
        private const val FAR = 100f
        private const val NEAR = 1f
        private const val POSITION_Z = 100f
    }
}