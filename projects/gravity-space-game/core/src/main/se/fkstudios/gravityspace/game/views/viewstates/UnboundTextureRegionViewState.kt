package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2

class UnboundTextureRegionViewState(
        override val textureRegion: TextureRegion,
        private var renderPriority: Int,
        private var width: Float,
        private var height: Float,
        private var position: Vector2,
        private var origin: Vector2,
        private var scale: Vector2 = Vector2(1f, 1f),
        private var rotation: Float = 0f
) : TextureRegionViewState {

    override fun renderPriority(): Int =
            renderPriority

    override fun screenWidth(): Float =
            width

    override fun screenHeight(): Float =
            height

    override fun screenPosition(): Vector2 =
            position

    override fun screenOrigin(): Vector2 =
            origin

    override fun scale(): Vector2 =
            scale

    override fun rotation(): Float =
            rotation
}