package se.fkstudios.gravityspace.game.models.world

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.World
import se.fkstudios.gravityspace.common.utils.GdxArray
import se.fkstudios.gravityspace.common.utils.setSimpleContactListener
import se.fkstudios.gravityspace.common.utils.vectorLen
import se.fkstudios.gravityspace.common.utils.DelayedParamJobTimer
import se.fkstudios.gravityspace.game.models.placeInOrbit
import se.fkstudios.gravityspace.game.models.world.objects.Missile
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class GravityWorld {

    val box2dWorld: World

    private val gravityConstant: Float
    private val radius: Float

    private val gravityUpdater: GravityUpdater
    private val missileTimers: GdxArray<DelayedParamJobTimer<WorldObject>> = GdxArray()
    private val tempMissileTimers: GdxArray<DelayedParamJobTimer<WorldObject>> = GdxArray()

    private var onDispose: (worldObject: WorldObject) -> Unit
    private var onOutsideWorld: (worldObject: WorldObject) -> Unit

    private lateinit var worldObjects: GdxArray<WorldObject>
    private lateinit var mostMassive: WorldObject
    private lateinit var team1SpawnLocation: WorldObject
    private lateinit var team2SpawnLocation: WorldObject

    private var tempWorldObjects: GdxArray<WorldObject> = GdxArray()

    constructor(
            box2dWorld: World,
            gravityConstant: Float,
            radius: Float,
            onContact: (contact: Contact) -> Unit,
            onOutsideWorld: (worldObject: WorldObject) -> Unit,
            onDispose: (worldObject: WorldObject) -> Unit,
            gravityUpdater: GravityUpdater = GravityUpdater()
    ) {
        this.box2dWorld = box2dWorld
        this.gravityConstant = gravityConstant
        this.gravityUpdater = gravityUpdater
        this.radius = radius
        box2dWorld.setSimpleContactListener(onContact)
        this.onOutsideWorld = onOutsideWorld
        this.onDispose = onDispose
    }

    fun initWorldObjects(
            worldObjects: GdxArray<WorldObject>,
            mostMassive: WorldObject,
            team1SpawnLocation: WorldObject,
            team2SpawnLocation: WorldObject
    ) {
        this.worldObjects = GdxArray(worldObjects)
        this.mostMassive = mostMassive
        this.team1SpawnLocation = team1SpawnLocation
        this.team2SpawnLocation = team2SpawnLocation
    }

    fun update(timeDelta: Float) {
        updateMissileTimers(timeDelta)
        gravityUpdater.update(worldObjects, mostMassive, gravityConstant, timeDelta)
        box2dWorld.step(timeDelta, 4, 2)
        removeObjectOutsideWorld()
    }

    private fun updateMissileTimers(timeDelta: Float) {
        tempMissileTimers.clear()
        tempMissileTimers.addAll(missileTimers)
        for (i in 0 until tempMissileTimers.size) {
            tempMissileTimers[i].update(timeDelta)
        }
    }

    private fun removeObjectOutsideWorld() {
        tempWorldObjects.clear()
        tempWorldObjects.addAll(worldObjects)
        for (i in 0 until tempWorldObjects.size) {
            val worldObject = tempWorldObjects[i]
            if (vectorLen(worldObject.position.x, worldObject.position.y) > radius) {
                onOutsideWorld(worldObject)
            }
        }
    }

    fun placeInOrbit(
            target: WorldObject,
            primary: WorldObject,
            distanceToPrimary: Float,
            angularOffset: Float,
            clockwise: Boolean = true
    ) {
        target.setPosition(primary.position.x, primary.position.y + distanceToPrimary)
        val dominatingWorldObject = target.dominating(mostMassive, primary, primary)
        val gravityMultiplier = 1f + target.dominatingGravityMultiplier(dominatingWorldObject, mostMassive)
        placeInOrbit(target, primary, distanceToPrimary, angularOffset, gravityConstant, gravityMultiplier, clockwise)
    }

    fun addSpaceship(spaceship: Spaceship, teamNo: Int) {
        worldObjects.add(spaceship)
        respawnSpaceship(spaceship, teamNo)
    }

    fun respawnSpaceship(spaceship: Spaceship, teamNo: Int) {
        System.out.println("Respawn ${spaceship.name}")
        spaceship.reset()
        val spawnLocation = getSpawnLocation(teamNo)
        val distanceToSpawner = spawnLocation.radius + MathUtils.random(5f, 10f)
        val angularOffset = MathUtils.random(0f, 360f)
        placeInOrbit(spaceship, spawnLocation, distanceToSpawner, angularOffset, true)
        spaceship.startIndestructibleTimer()
    }

    private fun getSpawnLocation(teamNo: Int): WorldObject = when (teamNo) {
        1 -> team1SpawnLocation
        2 -> team2SpawnLocation
        else -> throw Exception("Unsupported team")
    }

    fun addMissile(missile: WorldObject) {
        val missileTimer = DelayedParamJobTimer<WorldObject>()
        missileTimer.start(missile, 5f) {
            removeAndDispose(it)
        }
        missileTimers.add(missileTimer)
        worldObjects.add(missile)
    }

    fun removeAndDispose(worldObject: WorldObject) {
        worldObjects.removeValue(worldObject, true)
        worldObject.dispose()
        box2dWorld.destroyBody(worldObject.body)
        onDispose(worldObject)
        if (worldObject is Missile) {
            removeTimerFor(worldObject)
        }
    }

    fun dispose(): GdxArray<WorldObject> {
        missileTimers.forEach { it.reset() }
        missileTimers.clear()

        val disposedObjects = GdxArray<WorldObject>()
        while (worldObjects.size > 0) {
            val worldObject = worldObjects[0]
            removeAndDispose(worldObject)
            disposedObjects.add(worldObject)
        }

        box2dWorld.setSimpleContactListener(null)
        box2dWorld.dispose()
        return disposedObjects
    }

    private fun removeTimerFor(missile: Missile) {
        for (i in 0 until missileTimers.size) {
            if (missileTimers[i].parameter == missile) {
                missileTimers[i].reset()
                missileTimers.removeIndex(i)
                break
            }
        }
    }
}