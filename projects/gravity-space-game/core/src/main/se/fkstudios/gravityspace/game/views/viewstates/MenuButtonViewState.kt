package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.math.Rectangle
import se.fkstudios.gravityspace.common.views.darkGray
import se.fkstudios.gravityspace.common.views.lightGray
import se.fkstudios.gravityspace.game.controllers.GameState

class MenuButtonViewState(
        val activeGameState: GameState,
        rectangle: Rectangle,
        text: String,
        textSize: Int = 48,
        textColor: Color = lightGray,
        borderColor: Color = lightGray,
        fillColor: Color = Color(.0f, .0f, .0f, .8f)
) : ViewState {

    private val rectangle: Rectangle = Rectangle()

    private val glyphLayout: GlyphLayout

    val x: Float
        get() = rectangle.x

    val y: Float
        get() = rectangle.y

    val width: Float
        get() = rectangle.width

    val height: Float
        get() = rectangle.height

    val textX: Float
        get() = x + width / 2f - glyphLayout.width / 2f

    val textY: Float
        get() = y + height / 2f + glyphLayout.height / 2f

    val text: String = text

    val font: BitmapFont

    val borderColor = borderColor

    val fillColor = fillColor

    init {
        this.rectangle.set(rectangle)

        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.size = textSize
        parameter.color = textColor
        parameter.borderWidth = 3f
        parameter.borderColor = darkGray
        font = generator.generateFont(parameter)

        glyphLayout = GlyphLayout(font, text)
    }

    override fun renderPriority(): Int = 0
}