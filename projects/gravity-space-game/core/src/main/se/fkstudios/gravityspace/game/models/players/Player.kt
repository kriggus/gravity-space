package se.fkstudios.gravityspace.game.models.players

import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship

class Player(
        val clientId: Int,
        var teamNo: Int,
        var state: PlayerState
) {
    private val playerNames = arrayOf(
            "IRIS",
            "APOLLO",
            "EUROPA",
            "HECATE",
            "ARES",
            "HERA",
            "ATHENA",
            "KARES",
            "ARTEMIS",
            "NEMESIS",
            "ALKE",
            "KRATOS",
            "ALALA",
            "HERMES"
    )

    var spaceship: Spaceship? = null

    val name: String
        get() = playerNames[clientId % playerNames.size]

    var kills: Int = 0

    var deaths: Int = 0

    fun resetScore() {
        deaths = 0
        kills = 0
    }
}