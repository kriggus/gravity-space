package se.fkstudios.gravityspace.game.models.world.objects

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Shape
import se.fkstudios.gravityspace.common.utils.vectorLen
import se.fkstudios.gravityspace.common.utils.vectorNorX
import se.fkstudios.gravityspace.common.utils.vectorNorY
import se.fkstudios.gravityspace.common.utils.DelayedParamJobTimer
import se.fkstudios.gravityspace.game.models.world.objects.profiles.GravityProfile

class Spaceship : WorldObject {

    constructor(
            name: String,
            gravityProfile: GravityProfile,
            body: Body,
            shape: Shape,
            mass: Float,
            maxThrust: Float,
            maxSpeed: Float,
            fuelCapacity: Float,
            friction: Float = .3f,
            restitution: Float = .5f,
            startPosition: Vector2 = Vector2(0f, 0f),
            startVelocity: Vector2 = Vector2(0f, 0f),
            startAcceleration: Vector2 = Vector2(0f, 0f),
            startAngle: Float = 0f,
            startAngularVelocity: Float = 0f,
            indestructible: Boolean = false
    ) : super(
            name,
            gravityProfile,
            body,
            shape,
            mass,
            friction,
            restitution,
            startPosition,
            startVelocity,
            startAcceleration,
            startAngle,
            startAngularVelocity
    ) {
        this.indestructible = indestructible
        this.fuelCapacity = fuelCapacity
        this.fuel = fuelCapacity
        this.maxThrust = maxThrust
        this.maxSpeed = maxSpeed
        this.targetAngleDegrees = angleDegrees
    }

    fun reset() {
        setPosition(0f, 0f)
        setVelocity(0f, 0f)
        setAcceleration(0f, 0f)
        targetAngleDegrees = 0f
        angleDegrees = 0f
        angularVelocity = 0f
    }

    var indestructibleTimer = DelayedParamJobTimer<Spaceship>()

    var indestructible: Boolean
        private set

    fun startIndestructibleTimer() {
        indestructible = true
        indestructibleTimer.start(this, 3f) {
            System.out.println("$name is now destructible")
            it.indestructible = false
        }
    }

    var fuelCapacity: Float

    var fuel: Float

    val hasFuel: Boolean
        get() = fuel > 0f

    val maxSpeed: Float

    val hasThrust: Boolean
        get() = thrust.x != 0f || thrust.y != 0f

    var maxThrust: Float

    var thrust: Vector2 = Vector2(0f, 0f)
        set(value) {
            setThrust(value.x, value.y)
        }

    var targetAngleDegrees: Float

    var isAiming: Boolean = false

    fun setThrust(x: Float, y: Float) {
        if (x != 0f && y != 0f) {
            angularVelocity = 0f
        }
        thrust.x = x
        thrust.y = y

        if (thrust.len() > maxThrust) {
            thrust.scl(maxThrust / thrust.len())
        }
    }

    override fun update(timeDelta: Float) {
        indestructibleTimer.update(timeDelta)

        if (hasThrust && hasFuel) {
            updateAccelerationFromThrust()
            updateAngleDataFromThrust()
            consumeFuel(timeDelta)
        }

        super.update(timeDelta)

        angleDegrees = MathUtils.lerpAngleDeg(angleDegrees, targetAngleDegrees, 10f * timeDelta)
    }

    private fun consumeFuel(timeDelta: Float) {
        fuel -= (thrust.len() / maxThrust) * fuelCapacity * (timeDelta / MAX_THRUST_TIME_TO_CONSUME_ALL_FUEL)
        fuel = Math.max(0f, fuel)
    }

    private fun updateAngleDataFromThrust() {
        // Angle is measured from x-axis counter-clockwise, thus -90 to align object's angle with thrust's force direction
        // TODO Why the different angle systems? Should use the same angle def everywhere!
        targetAngleDegrees = thrust.angle() - 90f
        angularVelocity = 0f
    }

    private fun updateAccelerationFromThrust() {
        val frictionModifier = frictionModifier()
        val accelerationX = acceleration.x + thrustAccelerationX() + frictionAccelerationX(frictionModifier)
        val accelerationY = acceleration.y + thrustAccelerationY() + frictionAccelerationY(frictionModifier)
        setAcceleration(accelerationX, accelerationY)
    }

    private fun frictionModifier(): Float {
        val speedProportion = Math.min(1f, speed / maxSpeed)
        return Math.pow(speedProportion.toDouble(), 2 * Math.E).toFloat() // based on x^2e for x in [0, 1]
    }

    private fun thrustAccelerationX(): Float =
            thrust.x / mass

    private fun thrustAccelerationY(): Float =
            thrust.y / mass

    private fun frictionAccelerationX(modifier: Float): Float =
            -1 * vectorNorX(velocity.x, velocity.y) * vectorLen(thrust.x, thrust.y) * modifier

    private fun frictionAccelerationY(modifier: Float): Float =
            -1 * vectorNorY(velocity.x, velocity.y) * vectorLen(thrust.x, thrust.y) * modifier

    override fun dispose() {
        super.dispose()
        indestructibleTimer.reset()
    }

    companion object {
        private const val MAX_THRUST_TIME_TO_CONSUME_ALL_FUEL = 1000000f
    }
}