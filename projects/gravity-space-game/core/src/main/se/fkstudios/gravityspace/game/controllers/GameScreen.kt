package se.fkstudios.gravityspace.game.controllers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.utils.GdxScreen
import se.fkstudios.gravityspace.common.utils.heightAsFloat
import se.fkstudios.gravityspace.common.utils.widthAsFloat
import se.fkstudios.gravityspace.game.controllers.input.GameInputProcessor
import se.fkstudios.gravityspace.game.models.players.PlayerManager
import se.fkstudios.gravityspace.game.models.world.GravityWorld
import se.fkstudios.gravityspace.game.models.world.GravityWorldCreator
import se.fkstudios.gravityspace.game.network.NetworkManager
import se.fkstudios.gravityspace.game.network.ServerApi
import se.fkstudios.gravityspace.game.views.TextureManager
import se.fkstudios.gravityspace.game.views.invertY
import se.fkstudios.gravityspace.game.views.renderers.GameScreenRenderer
import se.fkstudios.gravityspace.game.views.viewstates.ViewStateManager

class GameScreen : GdxScreen() {

    private val textureManager = TextureManager()
    private val viewStateManager = ViewStateManager()
    private val playerManager = PlayerManager()

    private val renderer = GameScreenRenderer(
            Gdx.graphics.widthAsFloat,
            Gdx.graphics.heightAsFloat,
            viewStateManager,
            playerManager
    )

    private val camera = GameCamera(Gdx.graphics.widthAsFloat, Gdx.graphics.heightAsFloat)
    private val updater = GameUpdater(camera, playerManager)
    private val collisionHandler = CollisionHandler(playerManager) { Gdx.app.postRunnable(it) }

    private val menu = Menu(Gdx.graphics.width, Gdx.graphics.height, viewStateManager)
    private val worldCreator = GravityWorldCreator(textureManager, viewStateManager)
    private var world: GravityWorld = recreateWorld()

    private val networkApi: ServerApi = ServerApi(Gdx.app, NetworkManager.server).apply {
        initCallbacks(
                onReceivedThrust = { clientId, dragProportionX, dragProportionY ->
                    if (updater.isPlaying) {
                        val spaceship = playerManager.findPlayer(clientId)?.spaceship
                        if (spaceship != null) {
                            val maxThrust = spaceship.maxThrust
                            spaceship.setThrust(dragProportionX * maxThrust, dragProportionY * maxThrust)
                        }
                    }
                },
                onReceivedAngle = { clientId, x, y ->
                    if (updater.isPlaying) {
                        val spaceship = playerManager.findPlayer(clientId)?.spaceship
                        if (spaceship != null) {
                            spaceship.setThrust(0f, 0f)
                            // Angle is measured from x-axis counter-clockwise, thus -90 to align object's angle with angle input
                            // TODO Why the different angle systems? Should use the same angle def everywhere!
                            spaceship.targetAngleDegrees = Vector2(x, y).angle() - 90f
                            spaceship.angularVelocity = 0f
                        }
                    }
                },
                onReceivedFire = { clientId ->
                    if (updater.isPlaying) {
                        val player = playerManager.findPlayer(clientId)
                        val spaceship = player?.spaceship
                        if (spaceship != null) {
                            val missile = worldCreator.newMissile(world, spaceship, player.teamNo)
                            world.addMissile(missile)
                        }
                    }
                },
                onReceivedSelectTeam = { clientId, teamNo ->
                    if (updater.isLobby) {
                        playerManager.setTeam(clientId, teamNo)
                        sendTeamNoConfirmed(clientId, teamNo)
                    }
                },
                onReceivedIsAiming = { clientId, isAiming ->
                    if (updater.isPlaying) {
                        val spaceship = playerManager.findPlayer(clientId)?.spaceship
                        if (spaceship != null) {
                            spaceship.isAiming = isAiming
                        }
                    }
                },
                onClientConnected = { clientId ->
                    val playerState = if (updater.isLobby) PlayerState.LOBBY else PlayerState.WAITING
                    val player = playerManager.addNewPlayer(clientId, playerState)
                    sendPlayerName(clientId, player.name)
                    sendTeamNoConfirmed(clientId, player.teamNo)
                    sendPlayerState(clientId, playerState)
                },
                onClientDisconnected = { clientId ->
                    val player = playerManager.removePlayer(clientId)
                    val spaceship = player?.spaceship
                    if (spaceship != null) {
                        world.removeAndDispose(spaceship)
                        viewStateManager.unbindCameraStates(spaceship)
                    }
                }
        )
    }

    private val inputProcessor = GameInputProcessor(
            camera = camera,
            onClick = { screenX, screenY ->
                when {
                    updater.isLobby && menu.inStartHitArea(screenX, screenY.invertY()) -> {
                        disposeWorld()
                        recreateWorld()
                        updateStateToPlaying()
                    }
                    updater.isPaused && menu.inRestartHitArea(screenX, screenY.invertY()) -> {
                        disposeWorld()
                        recreateWorld()
                        updateStateToLobby()
                    }
                    updater.isPaused && menu.inResumeHitArea(screenX, screenY.invertY()) -> {
                        updateStateToPlaying()
                    }
                    updater.isPlaying -> {
                        updateStateToPaused()
                    }
                }
            }
    )

    override fun show() {
        camera.update()
        networkApi.startServer()
        Gdx.input.inputProcessor = inputProcessor
    }

    override fun resume() {
        Gdx.input.inputProcessor = inputProcessor
    }

    override fun update(timeDelta: Float) {
        updater.update(timeDelta)
    }

    override fun render() {
        renderer.render(camera, menu, updater.state)
    }

    override fun pause() {
        Gdx.input.inputProcessor = null
        if (!updater.isLobby) {
            updateStateToPaused()
        }
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        networkApi.stopServer()
        disposeWorld()
        renderer.dispose()
        viewStateManager.dispose()
        textureManager.dispose()
    }

    override fun resize(width: Int, height: Int) = Unit

    private fun recreateWorld(): GravityWorld {
        world = worldCreator.newWorld(
                onContact = { collisionHandler.handleContact(it) },
                onOutsideWorld = { collisionHandler.handleOutsideWorld(it) },
                onDispose = { viewStateManager.unbindCameraStates(it) }
        )

        for (player in playerManager.players) {
            worldCreator.newSpaceshipForPlayer(world, player)
            val spaceship = player.spaceship!!
            world.addSpaceship(spaceship, player.teamNo)
            player.resetScore()
        }

        updater.world = world
        collisionHandler.world = world
        return world
    }

    private fun disposeWorld() {
        world.dispose().forEach { viewStateManager.unbindCameraStates(it) }
    }

    private fun updateStateToLobby() {
        for (player in playerManager.players) {
            player.state = PlayerState.LOBBY
            networkApi.sendPlayerState(player.clientId, PlayerState.LOBBY)
        }
        updater.state = GameState.LOBBY
    }

    private fun updateStateToPlaying() {
        for (player in playerManager.players) {
            val gameIsInLobbyState = updater.state == GameState.LOBBY
            val playerIsNotWaiting = player.state != PlayerState.WAITING
            if (gameIsInLobbyState || playerIsNotWaiting) {
                player.state = PlayerState.PLAYING
                networkApi.sendPlayerState(player.clientId, PlayerState.PLAYING)
            }
        }
        updater.state = GameState.PLAYING
    }

    private fun updateStateToPaused() {
        for (player in playerManager.players) {
            val playerIsNotWaiting = player.state != PlayerState.WAITING
            if (playerIsNotWaiting) {
                player.state = PlayerState.PAUSED
                networkApi.sendPlayerState(player.clientId, PlayerState.PAUSED)
            }
        }
        updater.state = GameState.PAUSED
    }
}