package se.fkstudios.gravityspace.game

import com.badlogic.gdx.utils.PerformanceCounter

object PerformanceCounters {
    val updateAllCounter = PerformanceCounter("updateAllCounter")
    val updateModelCounter = PerformanceCounter("updateModelCounter")
    val renderCounter = PerformanceCounter("renderCounter")
}

fun PerformanceCounter.log() {
    System.out.println("$name : load=${load.average} : time=${time.average}")
}