package se.fkstudios.gravityspace.game.network

import com.badlogic.gdx.net.ServerSocket
import com.badlogic.gdx.net.Socket
import com.badlogic.gdx.utils.GdxRuntimeException
import se.fkstudios.gravityspace.common.network.SocketApi
import java.net.SocketTimeoutException
import kotlin.concurrent.thread

class Server(
        private val socketApi: SocketApi,
        private val newClientConnection: ClientConnectionCreator
) {
    private val lock: Any = Any()
    private var serverSocket: ServerSocket? = null
    private val connections: MutableMap<Int, ClientConnection> = mutableMapOf()
    private var connectionCounter: Int = 0
    private var running: Boolean = false

    private var onReceive: ServerReceiveHandler? = null
    private var onClientConnected: ClientConnectedHandler? = null
    private var onClientDisconnected: ClientDisconnectedHandler? = null

    fun start(
            onReceive: ServerReceiveHandler,
            onClientConnected: ClientConnectedHandler,
            onClientDisconnected: ClientDisconnectedHandler
    ) {
        synchronized(lock) {
            if (!running) {
                try {
                    running = true
                    this.onReceive = onReceive
                    this.onClientConnected = onClientConnected
                    this.onClientDisconnected = onClientDisconnected
                    serverSocket = socketApi.newServerSocket()
                    startAcceptClientsThread()
                } catch (e: GdxRuntimeException) {
                    System.out.println("Failed to start server. $e")
                    e.printStackTrace()
                }
            }
        }
    }

    fun stop() {
        synchronized(lock) {
            if (running) {
                connections.forEach { it.value.dispose() }
                connections.clear()
                serverSocket?.dispose()
                serverSocket = null
                running = false
            }
        }
    }

    fun send(clientId: Int, message: ByteArray) {
        synchronized(lock) {
            if (running) {
                val connection: ClientConnection? = connections[clientId]
                connection?.send(message)
            }
        }
    }

    fun sendToAll(message: ByteArray) {
        synchronized(lock) {
            if (running) {
                for (connection in connections.values) {
                    connection.send(message)
                }
            }
        }
    }

    private fun startAcceptClientsThread() {
        thread {
            while (running) {
                try {
                    serverSocket?.accept(socketApi.newClientHints())?.let { clientSocket ->
                        addNewClientConnection(clientSocket)
                    }
                } catch (e: GdxRuntimeException) {
                    if (e.cause == null || e.cause !is SocketTimeoutException) {
                        System.out.println("Failed to accept client connection. $e")
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun addNewClientConnection(clientSocket: Socket) {
        val onReceive = onReceive ?: throw GdxRuntimeException("onReceive was null")
        var connection: ClientConnection? = null
        synchronized(lock) {
            connection = newClientConnection(connectionCounter++, clientSocket) { client ->
                synchronized(lock) {
                    System.out.println("Disconnected client $client")
                    connections[client.id]?.dispose()
                    connections.remove(client.id)
                    onClientDisconnected?.invoke(client)
                }
            }
            connection?.let { connections.put(it.id, it) }
        }
        System.out.println("Connected client $connection")
        connection?.startReceiveThread(onReceive)
        connection?.let { onClientConnected?.invoke(it) }
    }
}