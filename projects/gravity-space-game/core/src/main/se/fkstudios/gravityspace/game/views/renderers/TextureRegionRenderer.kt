package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.TextureRegionViewState

class TextureRegionRenderer(
        private val spriteBatch: SpriteBatch
) : ViewStateRenderer<TextureRegionViewState> {

    override fun render(projectionMatrix: Matrix4, viewState: TextureRegionViewState) {
        spriteBatch.projectionMatrix = projectionMatrix
        spriteBatch.begin()
        val textureRegion = viewState.textureRegion
        val position = viewState.screenPosition()
        val origin = viewState.screenOrigin()
        val width = viewState.screenWidth()
        val height = viewState.screenHeight()
        val scale = viewState.scale()
        val rotation = viewState.rotation()
        spriteBatch.draw(
                textureRegion,
                position.x,
                position.y,
                origin.x,
                origin.y,
                width,
                height,
                scale.x,
                scale.y,
                rotation
        )
        spriteBatch.end()
    }
}