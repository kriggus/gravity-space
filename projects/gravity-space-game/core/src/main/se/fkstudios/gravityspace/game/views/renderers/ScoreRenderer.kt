package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.views.*
import se.fkstudios.gravityspace.game.models.players.Player
import se.fkstudios.gravityspace.game.models.players.PlayerManager

class ScoreRenderer(
        private val spriteBatch: SpriteBatch
) {
    private val greenFont: BitmapFont
    private val greenSmallFont: BitmapFont
    private val orangeFont: BitmapFont
    private val orangeSmallFont: BitmapFont

    init {
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontParameter()

        parameter.borderWidth = 3f
        parameter.size = 40
        parameter.color = team1Color
        parameter.borderColor = darkGray
        greenFont = generator.generateFont(parameter)

        parameter.borderWidth = 2f
        parameter.size = 20
        parameter.color = team1Color
        parameter.borderColor = darkGray
        greenSmallFont = generator.generateFont(parameter)

        parameter.borderWidth = 3f
        parameter.size = 40
        parameter.color = team2Color
        parameter.borderColor = darkGray
        orangeFont = generator.generateFont(parameter)

        parameter.borderWidth = 2f
        parameter.size = 20
        parameter.color = team2Color
        parameter.borderColor = darkGray
        orangeSmallFont = generator.generateFont(parameter)

        generator.dispose()
    }

    fun render(projectionMatrix: Matrix4, playerManager: PlayerManager) {
        spriteBatch.projectionMatrix = projectionMatrix
        spriteBatch.begin()

        val scoreTeam1 = playerManager.score(1)
        val scoreTeam2 = playerManager.score(2)

        greenFont.draw(
                spriteBatch,
                "TEAM $team1Name $scoreTeam1",
                Gdx.graphics.width * .025f,
                Gdx.graphics.height - Gdx.graphics.height * .05f,
                0f,
                -1,
                false
        )

        orangeFont.draw(
                spriteBatch,
                "TEAM $team2Name $scoreTeam2",
                Gdx.graphics.width - Gdx.graphics.width * .025f,
                Gdx.graphics.height - Gdx.graphics.height * .05f,
                0f,
                0,
                false
        )

        var greenIndex = 0
        var orangeIndex = 0

        for (player in playerManager.players) {
            if (player.state != PlayerState.WAITING) {
                when (player.teamNo) {
                    1 -> greenSmallFont.draw(
                            spriteBatch,
                            playerScoreInfo(player),
                            Gdx.graphics.width * .025f,
                            Gdx.graphics.height - Gdx.graphics.height * .05f - 60 - greenIndex++ * 30f
                    )
                    2 -> orangeSmallFont.draw(
                            spriteBatch,
                            playerScoreInfo(player),
                            Gdx.graphics.width - Gdx.graphics.width * .025f,
                            Gdx.graphics.height - Gdx.graphics.height * .05f - 60 - orangeIndex++ * 30f,
                            0f,
                            0,
                            false
                    )
                }
            }
        }
        spriteBatch.end()
    }

    private fun playerScoreInfo(player: Player): String =
            "${player.name} ${player.kills}/${player.deaths}"
}