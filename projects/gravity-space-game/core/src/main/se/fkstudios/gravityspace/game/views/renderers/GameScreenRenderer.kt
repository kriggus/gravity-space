package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.FPSLogger
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.common.views.renderers.ScreenRenderer
import se.fkstudios.gravityspace.game.controllers.GameCamera
import se.fkstudios.gravityspace.game.controllers.GameState
import se.fkstudios.gravityspace.game.controllers.Menu
import se.fkstudios.gravityspace.game.models.players.PlayerManager
import se.fkstudios.gravityspace.game.views.viewstates.*

class GameScreenRenderer(
        viewportWidth: Float,
        viewportHeight: Float,
        private val viewStateManager: ViewStateManager,
        private val playerManager: PlayerManager,
        private val spriteBatch: SpriteBatch = SpriteBatch(),
        private val shapeRenderer: ShapeRenderer = ShapeRenderer(),
        private val cameraTextureRenderer: TextureRegionRenderer = TextureRegionRenderer(spriteBatch),
        private val screenTextureRenderer: TextureRegionRenderer = TextureRegionRenderer(spriteBatch),
        private val thrustRenderer: ThrustRenderer = ThrustRenderer(shapeRenderer),
        private val aimHelperRenderer: AimHelperRenderer = AimHelperRenderer(shapeRenderer),
        private val scoreRenderer: ScoreRenderer = ScoreRenderer(spriteBatch),
        private val menuRenderer: MenuRenderer = MenuRenderer(spriteBatch, shapeRenderer),
        private val playerNamesRenderer: PlayerNameRenderer = PlayerNameRenderer(spriteBatch)
) : ScreenRenderer() {
    private val tempViewStates: MutableList<ViewState> = mutableListOf()

    private val cameraProjectionMatrix = Matrix4()
    private val screenProjectionMatrix = Matrix4()

    private val fpsLogger = FPSLogger()

    init {
        screenProjectionMatrix.setToOrtho2D(0f, 0f, viewportWidth, viewportHeight)
    }

    fun render(camera: GameCamera, menu: Menu, gameState: GameState) {
        cameraProjectionMatrix.set(camera.combined)

        clearScreen()
        renderBackground()
        renderCameraScene(camera)
        renderForeground()
        renderScore()
        renderMenu(gameState)

        fpsLogger.log()
    }

    private fun prepareViewStates(viewStates: List<ViewState>) {
        tempViewStates.clear()
        tempViewStates.addAll(viewStates)
        tempViewStates.sortByDescending { it.renderPriority() }
    }

    private fun renderBackground() {
        prepareViewStates(viewStateManager.backgroundStates())
        for (i in 0 until tempViewStates.size) {
            val viewState = tempViewStates[i]
            when (viewState) {
                is TextureRegionViewState -> screenTextureRenderer.render(screenProjectionMatrix, viewState)
                else -> System.out.println("renderBackground. Unsupported viewState ${viewState.javaClass.name}")
            }
        }
    }

    private fun renderCameraScene(camera: GameCamera) {
        prepareViewStates(viewStateManager.cameraStates())
        for (i in 0 until tempViewStates.size) {
            val viewState = tempViewStates[i]
            when (viewState) {
                is TextureRegionViewState -> cameraTextureRenderer.render(cameraProjectionMatrix, viewState)
                is ThrustViewState -> thrustRenderer.render(cameraProjectionMatrix, viewState)
                is AimHelperViewState -> aimHelperRenderer.render(cameraProjectionMatrix, viewState)
                is PlayerNameViewState -> playerNamesRenderer.render(camera, viewState)
                else -> System.out.println("renderCameraScene. Unsupported viewState ${viewState.javaClass.name}")
            }
        }
    }

    private fun renderForeground() {
        prepareViewStates(viewStateManager.foregroundStates())
        for (i in 0 until tempViewStates.size) {
            val viewState = tempViewStates[i]
            when (viewState) {
                is TextureRegionViewState -> screenTextureRenderer.render(screenProjectionMatrix, viewState)
                else -> System.out.println("renderForeground. Unsupported viewState ${viewState.javaClass.name}")
            }
        }
    }

    private fun renderScore() {
        scoreRenderer.render(screenProjectionMatrix, playerManager)
    }

    private fun renderMenu(gameState: GameState) {
        menuRenderer.render(screenProjectionMatrix, viewStateManager.menuButtonStates(), gameState)
    }

    fun dispose() {
        spriteBatch.dispose()
        shapeRenderer.dispose()
    }
}