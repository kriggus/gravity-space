package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.ViewState

interface ViewStateRenderer<T> where T : ViewState {
    fun render(projectionMatrix: Matrix4, viewState: T)
}