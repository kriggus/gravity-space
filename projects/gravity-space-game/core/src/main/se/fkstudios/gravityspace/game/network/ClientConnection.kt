package se.fkstudios.gravityspace.game.network

import com.badlogic.gdx.net.Socket
import se.fkstudios.gravityspace.common.network.messages.MessageParser
import se.fkstudios.gravityspace.common.network.messages.toLogString
import java.io.IOException
import kotlin.concurrent.thread

class ClientConnection(
        val id: Int,
        private val socket: Socket,
        private val onStop: (connection: ClientConnection) -> Unit
) {
    private var running: Boolean = false
    private var parser: MessageParser = MessageParser()
    private var payloadBuffer = ByteArray(65536)

    fun startReceiveThread(onReceiveMessage: ServerReceiveHandler) {
        if (!running) {
            running = true
            thread {
                while (running) {
                    try {
                        socket.inputStream?.let { stream ->
                            parser.parse(stream)
                            when (parser.stopped) {
                                true -> onStop(this)
                                else -> {
                                    val messageType = parser.type!!
                                    val payloadLength = parser.nextPayload(payloadBuffer)
                                    System.out.println("Received bytes from ${socket.remoteAddress} ${payloadBuffer.toLogString(messageType, payloadLength)}")
                                    onReceiveMessage(this, messageType, payloadBuffer, payloadLength)
                                }
                            }
                        }
                    } catch (e: IOException) {
                        System.out.println("Client connection stopped with $e")
                        e.printStackTrace()
                        onStop(this)
                    }
                }
            }
        }
    }

    fun send(message: ByteArray) {
        if (running) {
            try {
                System.out.println("Sending bytes to ${socket.remoteAddress}. ${message.toLogString()}")
                socket.outputStream?.write(message)
            } catch (e: IOException) {
                System.out.println("Error sending. $e")
                e.printStackTrace()
                onStop(this)
            }
        }
    }

    fun dispose() {
        if (running) {
            running = false
            socket.dispose()
        }
    }

    override fun toString(): String =
            "$id:${socket.remoteAddress}"
}