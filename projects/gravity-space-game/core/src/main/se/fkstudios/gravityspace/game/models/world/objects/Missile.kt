package se.fkstudios.gravityspace.game.models.world.objects

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Shape
import se.fkstudios.gravityspace.game.models.world.objects.profiles.GravityProfile

class Missile : WorldObject {

    val shooter: Spaceship

    constructor(
            name: String,
            gravityProfile: GravityProfile,
            body: Body,
            shape: Shape,
            mass: Float,
            friction: Float = .3f,
            restitution: Float = .5f,
            startPosition: Vector2 = Vector2(0f, 0f),
            startVelocity: Vector2 = Vector2(0f, 0f),
            startAcceleration: Vector2 = Vector2(0f, 0f),
            startAngle: Float = 0f,
            startAngularVelocity: Float = 0f,
            shooter: Spaceship
    ) : super(
            name,
            gravityProfile,
            body,
            shape,
            mass,
            friction,
            restitution,
            startPosition,
            startVelocity,
            startAcceleration,
            startAngle,
            startAngularVelocity
    ) {
        this.shooter = shooter
    }
}