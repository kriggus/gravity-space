package se.fkstudios.gravityspace.game.models

import com.badlogic.gdx.physics.box2d.Fixture
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

val Fixture.worldObject: WorldObject?
    get() = this.body?.userData as? WorldObject?