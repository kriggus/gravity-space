package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import se.fkstudios.gravityspace.common.utils.vectorRotateX
import se.fkstudios.gravityspace.common.utils.vectorRotateY
import se.fkstudios.gravityspace.common.views.clear
import se.fkstudios.gravityspace.common.views.darkGray
import se.fkstudios.gravityspace.common.views.red
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.views.toScreen

class AimHelperViewState(
        private val spaceship: Spaceship,
        private val renderPriority: Int
) : ViewState {

    private val primaryLineStart: Vector2 = Vector2()
    private val primaryLineEnd: Vector2 = Vector2()
    private val rightLineStart: Vector2 = Vector2()
    private val rightLineEnd: Vector2 = Vector2()
    private val leftLineStart: Vector2 = Vector2()
    private val leftLineEnd: Vector2 = Vector2()

    override fun renderPriority(): Int =
            renderPriority

    val thickness: Float
        get() = when (spaceship.isAiming) {
            true -> .2f.toScreen()
            false -> .16f.toScreen()
        }

    val primaryStartColor: Color
        get() = when (spaceship.isAiming) {
            true -> red
            false -> darkGray
        }

    val primaryEndColor: Color =
            clear

    val secondaryStartColor: Color
        get() = when (spaceship.isAiming) {
            true -> red
            false -> darkGray
        }

    val secondaryEndColor: Color
        get() = when (spaceship.isAiming) {
            true -> red
            false -> clear
        }

    fun primaryLineStart(): Vector2 {
        val degrees = spaceship.angleDegrees

        val nonRotatedDisplacementY = primaryLineDisplacementY()

        val displacementX = vectorRotateX(0f, nonRotatedDisplacementY, degrees)
        val displacementY = vectorRotateY(0f, nonRotatedDisplacementY, degrees)

        primaryLineStart.x = (spaceship.position.x + displacementX).toScreen()
        primaryLineStart.y = (spaceship.position.y + displacementY).toScreen()
        return primaryLineStart
    }

    fun primaryLineEnd(): Vector2 {
        val degrees = spaceship.angleDegrees
        val length = primaryLineLength()
        val displacementX = vectorRotateX(0f, spaceship.radius + length, degrees)
        val displacementY = vectorRotateY(0f, spaceship.radius + length, degrees)

        primaryLineEnd.x = (spaceship.position.x + displacementX).toScreen()
        primaryLineEnd.y = (spaceship.position.y + displacementY).toScreen()
        return primaryLineEnd
    }

    fun leftLineStart(): Vector2 {
        val degrees = spaceship.angleDegrees
        val displacementX = vectorRotateX(-spaceship.radius * .8f, spaceship.radius * .6f, degrees)
        val displacementY = vectorRotateY(-spaceship.radius * .8f, spaceship.radius * .6f, degrees)

        leftLineStart.x = (spaceship.position.x + displacementX).toScreen()
        leftLineStart.y = (spaceship.position.y + displacementY).toScreen()
        return leftLineStart
    }

    fun leftLineEnd(): Vector2 {
        val degrees = spaceship.angleDegrees
        val noneRotatedDisplacementX = secondaryLineEndDisplacementX()
        val secondaryLineLength = secondaryLineLength()
        val displacementX = vectorRotateX(-noneRotatedDisplacementX, spaceship.radius * .6f + secondaryLineLength, degrees)
        val displacementY = vectorRotateY(-noneRotatedDisplacementX, spaceship.radius * .6f + secondaryLineLength, degrees)

        leftLineEnd.x = (spaceship.position.x + displacementX).toScreen()
        leftLineEnd.y = (spaceship.position.y + displacementY).toScreen()
        return leftLineEnd
    }

    fun rightLineStart(): Vector2 {
        val degrees = spaceship.angleDegrees
        val displacementX = vectorRotateX(spaceship.radius * .8f, spaceship.radius * .6f, degrees)
        val displacementY = vectorRotateY(spaceship.radius * .8f, spaceship.radius * .6f, degrees)

        rightLineStart.x = (spaceship.position.x + displacementX).toScreen()
        rightLineStart.y = (spaceship.position.y + displacementY).toScreen()
        return rightLineStart
    }

    fun rightLineEnd(): Vector2 {
        val degrees = spaceship.angleDegrees
        val noneRotatedDisplacementX = secondaryLineEndDisplacementX()
        val secondaryLineLength = secondaryLineLength()
        val displacementX = vectorRotateX(noneRotatedDisplacementX, spaceship.radius * .6f + secondaryLineLength, degrees)
        val displacementY = vectorRotateY(noneRotatedDisplacementX, spaceship.radius * .6f + secondaryLineLength, degrees)

        rightLineEnd.x = (spaceship.position.x + displacementX).toScreen()
        rightLineEnd.y = (spaceship.position.y + displacementY).toScreen()
        return rightLineEnd
    }

    private fun primaryLineLength(): Float = when (spaceship.isAiming) {
        true -> 6f
        false -> 1.6f
    }

    private fun primaryLineDisplacementY(): Float = when (spaceship.isAiming) {
        true -> spaceship.radius * .6f + secondaryLineLength()
        false -> spaceship.radius * 1.1f
    }

    private fun secondaryLineLength(): Float = when (spaceship.isAiming) {
        true -> 1.3f
        false -> 1f
    }

    private fun secondaryLineEndDisplacementX(): Float = when (spaceship.isAiming) {
        true -> 0f
        false -> spaceship.radius * .8f
    }
}