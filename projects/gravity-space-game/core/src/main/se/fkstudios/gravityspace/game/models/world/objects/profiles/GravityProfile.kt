package se.fkstudios.gravityspace.game.models.world.objects.profiles

import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

interface GravityProfile {

    fun dominating(
            target: WorldObject,
            mostMassive: WorldObject?,
            mostInfluencing: WorldObject?,
            closestDominator: WorldObject?
    ): WorldObject?

    fun dominatingGravityMultiplier(
            target: WorldObject,
            dominating: WorldObject?,
            mostMassive: WorldObject?
    ): Float

}