package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2

interface TextureRegionViewState : ViewState {
    val textureRegion: TextureRegion
    fun screenWidth(): Float
    fun screenHeight(): Float
    fun screenPosition(): Vector2
    fun screenOrigin(): Vector2
    fun scale(): Vector2
    fun rotation(): Float
}