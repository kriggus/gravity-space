package se.fkstudios.gravityspace.game.models.world.objects.box2d

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Fixture
import se.fkstudios.gravityspace.game.models.density

class Box2dBodyCache(
        val body: Body,
        private val fixture: Fixture,
        startMass: Float
) {
    private val massCache = Box2dBodyFieldCache({ body.mass }, { updateMassData(it, radius) })
    var mass: Float
        get() = massCache.value
        set(value) {
            massCache.value = value
        }

    private val densityCache = Box2dBodyFieldCache({ fixture.density }, { })
    val density: Float
        get() = densityCache.value

    private val radiusCache = Box2dBodyFieldCache({ fixture.shape.radius }, { updateMassData(mass, it) })
    var radius: Float
        get() = radiusCache.value
        set(value) {
            radiusCache.value = value
        }

    private val positionCache = Box2dBodyFieldCache({ body.position }, { setPosition(it.x, it.y) })
    var position: Vector2
        get() = positionCache.value
        set(value) {
            positionCache.value = value
        }

    private val velocityCache = Box2dBodyFieldCache({ body.linearVelocity }, { setVelocity(it.x, it.y) })
    var velocity: Vector2
        get() = velocityCache.value
        set(value) {
            velocityCache.value = value
        }

    private val angleCache = Box2dBodyFieldCache({ body.angle }, { body.setTransform(position, it) })
    var angle: Float
        get() = angleCache.value
        set(value) {
            angleCache.value = value
        }

    private val angularVelocityCache = Box2dBodyFieldCache({ body.angularVelocity }, { body.angularVelocity = it })
    var angularVelocity: Float
        get() = angularVelocityCache.value
        set(value) {
            angularVelocityCache.value = value
        }

    init {
        this.mass = startMass
    }

    fun setPosition(x: Float, y: Float) {
        body.setTransform(x, y, angle)
        positionCache.invalidate()
    }

    fun setVelocity(x: Float, y: Float) {
        body.setLinearVelocity(x, y)
        velocityCache.invalidate()
    }

    fun invalidate() {
        massCache.invalidate()
        densityCache.invalidate()
        radiusCache.invalidate()
        positionCache.invalidate()
        velocityCache.invalidate()
        angleCache.invalidate()
        angularVelocityCache.invalidate()
    }

    private fun updateMassData(mass: Float, radius: Float) {
        fixture.shape.radius = radius
        fixture.density = density(mass, radius)
        fixture.body.resetMassData()
        massCache.invalidate()
        densityCache.invalidate()
        radiusCache.invalidate()
    }
}