package se.fkstudios.gravityspace.game.network

import com.badlogic.gdx.Gdx
import se.fkstudios.gravityspace.common.network.SocketApi

object NetworkManager {
    private val socketApi = SocketApi(Gdx.net)
    val server = Server(socketApi) { id, socket, onStop ->
        ClientConnection(id, socket, onStop)
    }
}