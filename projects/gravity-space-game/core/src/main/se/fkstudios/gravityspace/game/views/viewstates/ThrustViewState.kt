package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import se.fkstudios.gravityspace.common.utils.vectorRotateX
import se.fkstudios.gravityspace.common.utils.vectorRotateY
import se.fkstudios.gravityspace.common.views.iceBlue
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.views.toScreen

class ThrustViewState(
        private val spaceship: Spaceship,
        private val renderPriority: Int
) : ViewState {

    private val start: Vector2 = Vector2()
    private val end: Vector2 = Vector2()

    override fun renderPriority(): Int =
            renderPriority

    val thickness: Float =
            .15f.toScreen()

    val color: Color =
            iceBlue

    val shouldRender: Boolean
        get() = spaceship.hasThrust

    fun start(): Vector2 {
        val degrees = spaceship.angleDegrees
        val displacementX = vectorRotateX(-spaceship.radius * .4f, -spaceship.radius * 1.05f, degrees)
        val displacementY = vectorRotateY(-spaceship.radius * .4f, -spaceship.radius * 1.05f, degrees)

        start.x = (spaceship.position.x + displacementX).toScreen()
        start.y = (spaceship.position.y + displacementY).toScreen()
        return start
    }

    fun end(): Vector2 {
        val degrees = spaceship.angleDegrees
        val displacementX = vectorRotateX(spaceship.radius * .4f, -spaceship.radius * 1.05f, degrees)
        val displacementY = vectorRotateY(spaceship.radius * .4f, -spaceship.radius * 1.05f, degrees)

        end.x = (spaceship.position.x + displacementX).toScreen()
        end.y = (spaceship.position.y + displacementY).toScreen()
        return end
    }
}