package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject
import se.fkstudios.gravityspace.game.views.toScreen

class BoundTextureRegionViewState<T>(
        override val textureRegion: TextureRegion,
        val model: T,
        private var renderPriority: Int,
        private var scaleModifier: Float = 1f,
        private var positionOffset: Vector2 = Vector2(0f, 0f)
) : TextureRegionViewState where T : WorldObject {

    private val position: Vector2 = Vector2()
    private val origin: Vector2 = Vector2()
    private val scale: Vector2 = Vector2()

    override fun renderPriority(): Int =
            renderPriority

    override fun screenWidth(): Float =
            model.width.toScreen()

    override fun screenHeight(): Float =
            model.height.toScreen()

    override fun screenPosition(): Vector2 {
        position.x = model.position.x.toScreen() - screenWidth() / 2f
        position.y = model.position.y.toScreen() - screenHeight() / 2f
        return position
    }

    override fun screenOrigin(): Vector2 {
        origin.x = screenWidth() / 2f + positionOffset.x.toScreen()
        origin.y = screenHeight() / 2f + positionOffset.y.toScreen()
        return origin
    }

    override fun scale(): Vector2 {
        scale.x = scaleModifier
        scale.y = scaleModifier
        return scale
    }

    override fun rotation(): Float =
            model.angleDegrees
}