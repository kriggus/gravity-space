package se.fkstudios.gravityspace.game.models.world.objects

class GravityCache {

    var mostInfluencing: WorldObject? = null
        private set

    var mostInfluencingForce: Float? = null
        private set

    var closestDominator: WorldObject? = null
        private set

    var closestDominatorDistance: Float? = null
        private set

    fun invalidate() {
        closestDominator = null
        closestDominatorDistance = null
        mostInfluencing = null
        mostInfluencingForce = null
    }

    fun updateMostInfluencing(contender: WorldObject, forceAmountBetween: Float) {
        if (forceAmountBetween > mostInfluencingForce ?: Float.MIN_VALUE) {
            mostInfluencingForce = forceAmountBetween
            mostInfluencing = contender
        }
    }

    fun updateClosestDominator(contender: WorldObject, distanceBetween: Float) {
        if (contender.isDominator && distanceBetween < closestDominatorDistance ?: Float.MAX_VALUE) {
            closestDominatorDistance = distanceBetween
            closestDominator = contender
        }
    }
}