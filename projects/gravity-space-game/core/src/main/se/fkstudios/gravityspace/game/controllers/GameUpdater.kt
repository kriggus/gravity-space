package se.fkstudios.gravityspace.game.controllers

import se.fkstudios.gravityspace.game.models.players.PlayerManager
import se.fkstudios.gravityspace.game.models.world.GravityWorld

class GameUpdater(
        private val camera: GameCamera,
        private val playerManager: PlayerManager,
        var state: GameState = GameState.LOBBY
) {
    lateinit var world: GravityWorld

    val isPaused: Boolean
        get() = state == GameState.PAUSED

    val isLobby: Boolean
        get() = state == GameState.LOBBY

    val isPlaying
        get() = state == GameState.PLAYING

    private var timeDeltaToUpdate = 0f

    fun update(timeDelta: Float) {
        if (isPlaying) {
            timeDeltaToUpdate += timeDelta
            val stepLength = selectStepLength()
            if (stepLength > 0f) {
                world.update(stepLength)
                camera.update(stepLength, state, playerManager.players)
                timeDeltaToUpdate -= stepLength
            }
        }
    }

    private fun selectStepLength(): Float = when {
        timeDeltaToUpdate >= TARGET_STEP * 10f -> CATCH_UP_VERY_FAST_STEP
        timeDeltaToUpdate >= TARGET_STEP * 5f -> CATCH_UP_FAST_STEP
        timeDeltaToUpdate >= TARGET_STEP * 2f -> CATCH_UP_SLOW_STEP
        timeDeltaToUpdate >= TARGET_STEP -> TARGET_STEP
        else -> 0f
    }

    companion object {
        private const val TARGET_STEP = 1f / 60f
        private const val CATCH_UP_SLOW_STEP = 1f / 55f
        private const val CATCH_UP_FAST_STEP = 1f / 45f
        private const val CATCH_UP_VERY_FAST_STEP = 1f / 30f
    }
}