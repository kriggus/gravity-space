package se.fkstudios.gravityspace.game.views.viewstates

import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class ViewStateManager {

    private val boundCameraStateMap: MutableMap<WorldObject, MutableList<ViewState>> = mutableMapOf()
    private val unboundCameraStates: MutableList<ViewState> = mutableListOf()
    private val backgroundStates: MutableList<ViewState> = mutableListOf()
    private val foregroundStates: MutableList<ViewState> = mutableListOf()
    private val menuButtonStates: MutableList<MenuButtonViewState> = mutableListOf()

    fun cameraStates(): List<ViewState> =
            unboundCameraStates + boundCameraStateMap.values.flatten()

    fun backgroundStates(): List<ViewState> =
            backgroundStates

    fun foregroundStates(): List<ViewState> =
            foregroundStates

    fun menuButtonStates(): List<MenuButtonViewState> =
            menuButtonStates

    fun bindCameraState(model: WorldObject, viewState: ViewState) {
        if (boundCameraStateMap[model] == null) boundCameraStateMap[model] = mutableListOf()
        boundCameraStateMap[model]?.add(viewState)
    }

    fun bindCameraStates(model: WorldObject, viewStates: List<ViewState>) {
        if (boundCameraStateMap[model] == null) boundCameraStateMap[model] = mutableListOf()
        boundCameraStateMap[model]?.addAll(viewStates)
    }

    fun unbindCameraState(model: WorldObject, viewState: ViewState): Boolean {
        return boundCameraStateMap[model]?.remove(viewState) == true
    }

    fun unbindCameraStates(model: WorldObject): List<ViewState> {
        return boundCameraStateMap.remove(model) ?: emptyList()
    }

    fun addUnboundCameraState(viewState: ViewState) {
        unboundCameraStates.add(viewState)
    }

    fun removeUnboundCameraState(viewState: ViewState) {
        unboundCameraStates.remove(viewState)
    }

    fun addScreenBackgroundState(viewState: ViewState) {
        backgroundStates.add(viewState)
    }

    fun removeScreenBackgroundState(viewState: ViewState) {
        backgroundStates.remove(viewState)
    }

    fun addScreenForegroundState(viewState: ViewState) {
        foregroundStates.add(viewState)
    }

    fun removeScreenForegroundState(viewState: ViewState) {
        foregroundStates.remove(viewState)
    }

    fun addMenuButtonState(viewState: MenuButtonViewState) {
        menuButtonStates.add(viewState)
    }

    fun removeMenuButtonState(viewState: MenuButtonViewState) {
        menuButtonStates.remove(viewState)
    }

    fun dispose() {
        boundCameraStateMap.clear()
        unboundCameraStates.clear()
        backgroundStates.clear()
        foregroundStates.clear()
        menuButtonStates.clear()
    }
}