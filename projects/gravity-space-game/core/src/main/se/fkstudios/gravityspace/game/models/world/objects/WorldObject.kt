package se.fkstudios.gravityspace.game.models.world.objects

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.Shape
import se.fkstudios.gravityspace.game.models.world.objects.box2d.Box2dBodyCache
import se.fkstudios.gravityspace.game.models.world.objects.profiles.GravityProfile

open class WorldObject {

    private val cache: Box2dBodyCache
    private val gravityCache: GravityCache
    private val gravityProfile: GravityProfile

    val name: String

    var isDominator: Boolean

    val body: Body
        get() = cache.body

    var mass: Float
        get() = cache.mass
        set(value) {
            cache.mass = value
        }

    val density: Float
        get() = cache.density

    val width: Float
        get() = radius * 2f

    val height: Float
        get() = radius * 2f

    var radius: Float
        get() = cache.radius
        set(value) {
            cache.radius = value
        }

    var position: Vector2
        get() = cache.position
        set(value) {
            cache.position = value
        }

    var velocity: Vector2
        get() = cache.velocity
        set(value) {
            cache.velocity = value
        }

    val speed: Float
        get() = velocity.len()

    var acceleration: Vector2 = Vector2()
        set(value) {
            setAcceleration(value.x, value.y)
        }

    var angleDegrees: Float
        get() = cache.angle * MathUtils.radiansToDegrees
        set(value) {
            cache.angle = value / MathUtils.radiansToDegrees
        }

    var angularVelocity: Float
        get() = cache.angularVelocity
        set(value) {
            cache.angularVelocity = value
        }

    constructor(
            name: String,
            gravityProfile: GravityProfile,
            body: Body,
            shape: Shape,
            mass: Float,
            friction: Float = .5f,
            restitution: Float = .5f,
            startPosition: Vector2 = Vector2(0f, 0f),
            startVelocity: Vector2 = Vector2(0f, 0f),
            startAcceleration: Vector2 = Vector2(0f, 0f),
            startAngle: Float = 0f,
            startAngularVelocity: Float = 0f,
            isDominator: Boolean = false,
            gravityCache: GravityCache = GravityCache()) {
        this.name = name
        this.isDominator = isDominator
        this.gravityProfile = gravityProfile
        this.gravityCache = gravityCache
        val fixture = body.createFixture(FixtureDef().also {
            it.shape = shape
            it.friction = friction
            it.restitution = restitution
        })
        cache = Box2dBodyCache(body, fixture, mass)
        position = startPosition
        velocity = startVelocity
        acceleration = startAcceleration
        angleDegrees = startAngle
        angularVelocity = startAngularVelocity
        body.userData = this
    }

    fun setPosition(x: Float, y: Float) {
        cache.setPosition(x, y)
    }

    fun setVelocity(x: Float, y: Float) {
        cache.setVelocity(x, y)
    }

    fun setAcceleration(x: Float, y: Float) {
        acceleration.x = x
        acceleration.y = y
    }

    fun addAcceleration(x: Float, y: Float) {
        acceleration.x += x
        acceleration.y += y
    }

    fun dominating(mostMassive: WorldObject?, mostInfluencing: WorldObject?, closestDominator: WorldObject?): WorldObject? =
            gravityProfile.dominating(this, mostMassive, mostInfluencing, closestDominator)

    fun dominatingFromCache(mostMassive: WorldObject?): WorldObject? =
            gravityProfile.dominating(this, mostMassive, gravityCache.mostInfluencing, gravityCache.closestDominator)

    fun dominatingGravityMultiplier(dominating: WorldObject?, mostMassive: WorldObject?): Float =
            gravityProfile.dominatingGravityMultiplier(this, dominating, mostMassive)

    fun updateClosestDominator(contender: WorldObject, distanceBetween: Float) {
        gravityCache.updateClosestDominator(contender, distanceBetween)
    }

    fun updateMostInfluencing(contender: WorldObject, distanceBetween: Float) {
        gravityCache.updateMostInfluencing(contender, distanceBetween)
    }

    open fun update(timeDelta: Float) {
        val newVelocityX = velocity.x + acceleration.x * timeDelta
        val newVelocityY = velocity.y + acceleration.y * timeDelta
        setVelocity(newVelocityX, newVelocityY)
    }

    fun invalidateCache() {
        cache.invalidate()
        gravityCache.invalidate()
    }

    open fun dispose() {
        body.userData = null
    }
}