package se.fkstudios.gravityspace.game.models.world.objects.box2d

class Box2dBodyFieldCache<T>(
        private val getValue: (() -> T),
        private val setValue: ((value: T) -> Unit)
) {
    private var invalidCache: Boolean = true
    private var cachedValue: T? = null

    var value: T
        get() {
            if (invalidCache) {
                cachedValue = getValue()
                invalidCache = false
            }
            return cachedValue ?: getValue()
        }
        set(value) {
            setValue(value)
            invalidCache = true
        }

    fun invalidate() {
        invalidCache = true
    }
}