package se.fkstudios.gravityspace.game.controllers

import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.Fixture
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.game.models.players.Player
import se.fkstudios.gravityspace.game.models.players.PlayerManager
import se.fkstudios.gravityspace.game.models.world.GravityWorld
import se.fkstudios.gravityspace.game.models.world.objects.Missile
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject
import se.fkstudios.gravityspace.game.models.worldObject

class CollisionHandler(
        private val playerManager: PlayerManager,
        private val runDelayed: (() -> Unit) -> Unit
) {
    lateinit var world: GravityWorld

    fun handleOutsideWorld(worldObject: WorldObject) {
        when (worldObject) {
            is Spaceship -> respawnSpaceship(worldObject)
            else -> destroyObject(worldObject)
        }
    }

    fun handleContact(contact: Contact) {
        if (contact.isTouching) {
            val objectA = findObjectFromFixture(contact.fixtureA) ?: return
            val objectB = findObjectFromFixture(contact.fixtureB) ?: return
            runDelayed {
                handleHitBy(objectA, objectB)
                handleHitBy(objectB, objectA)
            }
        }
    }

    private fun handleHitBy(hitted: WorldObject, hitter: WorldObject) {
        if (hitted is Spaceship && hitter is Missile) missileHitSpaceship(hitted, hitter)
        if (hitted is Spaceship) respawnSpaceship(hitted)
        if (hitted is Missile) destroyObject(hitted)
    }

    private fun respawnSpaceship(spaceship: Spaceship) {
        if (!spaceship.indestructible) {
            val player = findPlayerFromSpaceship(spaceship)
            if (player != null && player.state == PlayerState.PLAYING) {
                player.deaths++
                world.respawnSpaceship(spaceship, player.teamNo)
            }
        }
    }

    private fun missileHitSpaceship(spaceship: Spaceship, missile: Missile) {
        if (!spaceship.indestructible) {
            val shootingPlayer = findPlayerFromSpaceship(missile.shooter) ?: return
            val hittedPlayer = findPlayerFromSpaceship(spaceship) ?: return

            if (shootingPlayer.teamNo != hittedPlayer.teamNo) {
                shootingPlayer.kills++
            }
        }
    }

    private fun destroyObject(worldObject: WorldObject) {
        world.removeAndDispose(worldObject)
    }

    private fun findPlayerFromSpaceship(spaceship: Spaceship): Player? {
        val player = playerManager.findPlayer(spaceship)
        if (player == null) {
            System.out.println("Collision handling error! Could not find player for spaceship ${spaceship.name}")
        }
        return player
    }

    private fun findObjectFromFixture(fixture: Fixture): WorldObject? {
        val obj = fixture.worldObject
        if (obj == null) {
            System.out.println("Collision handling error! Could not find player for fixture")
        }
        return obj
    }
}