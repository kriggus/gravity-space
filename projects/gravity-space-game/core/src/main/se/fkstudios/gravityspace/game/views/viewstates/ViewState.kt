package se.fkstudios.gravityspace.game.views.viewstates

interface ViewState {
    fun renderPriority(): Int
}