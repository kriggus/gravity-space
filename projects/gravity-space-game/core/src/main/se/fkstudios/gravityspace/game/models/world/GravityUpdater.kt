package se.fkstudios.gravityspace.game.models.world

import com.badlogic.gdx.utils.Array
import se.fkstudios.gravityspace.common.utils.GdxArray
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject

class GravityUpdater(
        private val forceCalculator: GravityForceCalculator = GravityForceCalculator()
) {
    private val notUpdated = GdxArray<WorldObject>()

    private fun addAccelerationFromForceCalculation(target: WorldObject) {
        target.addAcceleration(forceCalculator.forceBetweenX / target.mass, forceCalculator.forceBetweenY / target.mass)
        forceCalculator.influencing?.let { influencing ->
            target.updateClosestDominator(influencing, forceCalculator.distanceBetween)
            target.updateMostInfluencing(influencing, forceCalculator.forceAmountBetween)
        }
    }

    private fun addGravityAccelerations(target: WorldObject, influcensers: Array<WorldObject>, gravityConstant: Float) {
        for (i in 0 until influcensers.size) {
            val influencing = influcensers[i]
            if (target != influencing) {
                forceCalculator.calculateForce(target, influencing, gravityConstant, 1f)
                addAccelerationFromForceCalculation(target)
                forceCalculator.reverse()
                addAccelerationFromForceCalculation(influencing)
            }
        }
    }

    private fun addGravityAccelerationModifications(target: WorldObject, mostMassive: WorldObject?, gravityConstant: Float) {
        target.dominatingFromCache(mostMassive)?.let { dominating ->
            val multiplier = target.dominatingGravityMultiplier(dominating, mostMassive)
            forceCalculator.calculateForce(target, dominating, gravityConstant, multiplier)
            addAccelerationFromForceCalculation(target)
        }
    }

    fun update(worldObjects: GdxArray<WorldObject>, mostMassive: WorldObject, gravityConstant: Float, timeDelta: Float) {
        for (i in 0 until worldObjects.size) {
            worldObjects[i].invalidateCache()
            worldObjects[i].setAcceleration(0f, 0f)
        }

        notUpdated.clear()
        notUpdated.addAll(worldObjects)
        while (notUpdated.size > 0) {
            addGravityAccelerations(notUpdated[0], notUpdated, gravityConstant)
            notUpdated.removeIndex(0)
        }

        for (i in 0 until worldObjects.size) {
            addGravityAccelerationModifications(worldObjects[i], mostMassive, gravityConstant)
            worldObjects[i].update(timeDelta)
        }
    }
}