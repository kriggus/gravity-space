package se.fkstudios.gravityspace.game.models

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

fun adjustScalarForPeriodicity(scalar: Float, periodicLength: Float): Float = when {
    scalar > periodicLength -> adjustScalarForPeriodicity(scalar - periodicLength, periodicLength)
    scalar < 0 -> adjustScalarForPeriodicity(scalar + periodicLength, periodicLength)
    else -> scalar
}

fun Float.adjustForPeriodicity(periodicLength: Float): Float =
        adjustScalarForPeriodicity(this, periodicLength)

fun Vector2.adjustForPeriodicity(periodicLengthX: Float, periodicLengthY: Float) {
    x = adjustScalarForPeriodicity(x, periodicLengthX)
    y = adjustScalarForPeriodicity(y, periodicLengthY)
}

fun Vector3.adjustForPeriodicity(periodicLengthX: Float, periodicLengthY: Float, periodicLengthZ: Float) {
    x = adjustScalarForPeriodicity(x, periodicLengthX)
    y = adjustScalarForPeriodicity(y, periodicLengthY)
    z = adjustScalarForPeriodicity(z, periodicLengthZ)
}

fun Vector3.adjustForPeriodicity(periodicLengthX: Float, periodicLengthY: Float) {
    x = adjustScalarForPeriodicity(x, periodicLengthX)
    y = adjustScalarForPeriodicity(y, periodicLengthY)
}

