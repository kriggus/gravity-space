package se.fkstudios.gravityspace.game.network

import com.badlogic.gdx.Application
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.network.messages.MessageTypes
import se.fkstudios.gravityspace.common.network.messages.newMessage


class ServerApi(
        private val app: Application,
        private val server: Server
) {
    private lateinit var onReceivedThrust: (connectionId: Int, dragProportionX: Float, dragProportionY: Float) -> Unit
    private lateinit var onReceivedAngle: (connectionId: Int, x: Float, y: Float) -> Unit
    private lateinit var onReceivedFire: (connectionId: Int) -> Unit
    private lateinit var onReceivedSelectTeam: (connectionId: Int, teamNo: Int) -> Unit
    private lateinit var onReceivedIsAiming: (connectionId: Int, isAiming: Boolean) -> Unit
    private lateinit var onClientConnected: (connectionId: Int) -> Unit
    private lateinit var onClientDisconnected: (connectionId: Int) -> Unit

    fun initCallbacks(
            onReceivedThrust: (connectionId: Int, dragProportionX: Float, dragProportionY: Float) -> Unit,
            onReceivedAngle: (connectionId: Int, x: Float, y: Float) -> Unit,
            onReceivedFire: (connectionId: Int) -> Unit,
            onReceivedSelectTeam: (connectionId: Int, teamNo: Int) -> Unit,
            onReceivedIsAiming: (connectionId: Int, isAiming: Boolean) -> Unit,
            onClientConnected: (connectionId: Int) -> Unit,
            onClientDisconnected: (connectionId: Int) -> Unit
    ) {
        this.onReceivedThrust = onReceivedThrust
        this.onReceivedAngle = onReceivedAngle
        this.onReceivedFire = onReceivedFire
        this.onReceivedSelectTeam = onReceivedSelectTeam
        this.onReceivedIsAiming = onReceivedIsAiming
        this.onClientConnected = onClientConnected
        this.onClientDisconnected = onClientDisconnected
    }

    private val handleReceived = { connection: ClientConnection, type: Int, payloadBuffer: ByteArray, _: Int ->
        when (type) {
            MessageTypes.thrust -> {
                val dragProportionX = payloadBuffer[0].toFloat() / 100f
                val dragProportionY = payloadBuffer[1].toFloat() / 100f
                app.postRunnable {
                    onReceivedThrust(connection.id, dragProportionX, dragProportionY)
                }
            }
            MessageTypes.angle -> {
                val x = payloadBuffer[0].toFloat() / 100f
                val y = payloadBuffer[1].toFloat() / 100f
                app.postRunnable {
                    onReceivedAngle(connection.id, x, y)
                }
            }
            MessageTypes.fire -> {
                app.postRunnable {
                    onReceivedFire(connection.id)
                }
            }
            MessageTypes.isAiming -> {
                val isAiming = payloadBuffer[0].toInt() == 1
                app.postRunnable {
                    onReceivedIsAiming(connection.id, isAiming)
                }
            }
            MessageTypes.selectTeam -> {
                val teamNo = payloadBuffer[0].toInt()
                app.postRunnable {
                    onReceivedSelectTeam(connection.id, teamNo)
                }
            }
        }
    }

    private val handleClientConnected = { connection: ClientConnection ->
        app.postRunnable {
            onClientConnected(connection.id)
        }
    }

    private val handleClientDisconnected = { connection: ClientConnection ->
        app.postRunnable {
            onClientDisconnected(connection.id)
        }
    }

    fun sendTeamNoConfirmed(clientId: Int, teamNo: Int) {
        val message = newConfirmTeamMessage(teamNo)
        server.send(clientId, message)
    }

    fun sendPlayerName(clientId: Int, name: String) {
        val message = newPlayerNameMessage(name)
        server.send(clientId, message)
    }

    fun sendPlayerState(clientId: Int, state: PlayerState) {
        val message = newPlayerStateMessage(state)
        server.send(clientId, message)
    }

    private fun newConfirmTeamMessage(teamNo: Int): ByteArray {
        val payload = ByteArray(1)
        payload[0] = teamNo.toByte()
        return newMessage(MessageTypes.confirmTeam, payload)
    }

    private fun newPlayerNameMessage(name: String): ByteArray {
        val payload = name.toByteArray(Charsets.UTF_8)
        return newMessage(MessageTypes.playerName, payload)
    }

    private fun newPlayerStateMessage(state: PlayerState): ByteArray {
        val payload = ByteArray(1)
        payload[0] = state.raw
        return newMessage(MessageTypes.playerState, payload)
    }

    fun startServer() {
        server.start(handleReceived, handleClientConnected, handleClientDisconnected)
    }

    fun stopServer() {
        server.stop()
    }
}