package se.fkstudios.gravityspace.game.views.renderers

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import se.fkstudios.gravityspace.game.views.viewstates.ThrustViewState

class ThrustRenderer(
        private val shapeRenderer: ShapeRenderer
) : ViewStateRenderer<ThrustViewState> {

    override fun render(projectionMatrix: Matrix4, viewState: ThrustViewState) {
        if (viewState.shouldRender) {
            val start = viewState.start()
            val end = viewState.end()

            shapeRenderer.projectionMatrix = projectionMatrix
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)

            shapeRenderer.color = viewState.color
            shapeRenderer.rectLine(start.x, start.y, end.x, end.y, viewState.thickness)

            shapeRenderer.end()
        }
    }
}