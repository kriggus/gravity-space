package se.fkstudios.gravityspace.game.views.viewstates

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import se.fkstudios.gravityspace.common.views.darkGray
import se.fkstudios.gravityspace.common.views.gray
import se.fkstudios.gravityspace.common.views.team1Color
import se.fkstudios.gravityspace.common.views.team2Color
import se.fkstudios.gravityspace.game.models.players.Player
import se.fkstudios.gravityspace.game.models.world.objects.Spaceship
import se.fkstudios.gravityspace.game.views.toScreen

class PlayerNameViewState(
        player: Player,
        private val spaceship: Spaceship
) : ViewState {

    override fun renderPriority(): Int = 1

    val font: BitmapFont

    val x: Float
        get() = (spaceship.position.x + 1.4f * spaceship.radius).toScreen()

    val y: Float
        get() = (spaceship.position.y - 0.6f * spaceship.radius).toScreen()

    val text: String = player.name

    val shouldRender: Boolean
        get() = !spaceship.isAiming

    init {
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.borderWidth = 2f
        parameter.borderColor = darkGray.cpy().also { it.a = .4f }
        parameter.size = 9
        parameter.minFilter = Texture.TextureFilter.Linear
        parameter.magFilter = Texture.TextureFilter.Linear
        parameter.color = when (player.teamNo) {
            1 -> team1Color.cpy().also { it.a = .4f }
            2 -> team2Color.cpy().also { it.a = .4f }
            else -> gray
        }

        font = generator.generateFont(parameter)

        generator.dispose()
    }
}