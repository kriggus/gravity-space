package game.models.world.objects

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import junit.framework.TestCase.assertEquals
import org.junit.Test
import se.fkstudios.gravityspace.game.models.world.objects.WorldObject
import se.fkstudios.gravityspace.game.models.world.objects.profiles.StrongerMostMassiveProfile
import kotlin.test.BeforeTest

class WorldObjectTest {

    lateinit var body: Body
    lateinit var staticBody: Body
    lateinit var circle100: Shape

    @BeforeTest
    fun initWorld() {
        val world = World(Vector2(0f, 0f), true)
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody
        body = world.createBody(bodyDef)
        val staticBodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.StaticBody
        staticBody = world.createBody(staticBodyDef)
        circle100 = CircleShape().apply { radius = 100f }
    }

    @Test
    fun `Constructor sets all fields`() {
        val startPosition = Vector2(10f, 10f)
        val startVelocity = Vector2(0.1f, 0f)
        val startAcceleration = Vector2(2f, 3f)
        val startAngle = 0.0f
        val startAngularVelocity = 0.0f

        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                10000f,
                0.2f,
                0.5f,
                startPosition,
                startVelocity,
                startAcceleration,
                startAngle,
                startAngularVelocity
        )

        assertEquals("name1", obj.name)
        assertEquals(10000f, obj.mass, 0.01f)
        assertEquals(0.3183f, obj.density, 0.01f)
        assertEquals(200f, obj.width, 0.01f)
        assertEquals(200f, obj.height, 0.01f)
        assertEquals(startPosition, obj.position)
        assertEquals(startVelocity, obj.velocity)
        assertEquals(startAcceleration, obj.acceleration)
        assertEquals(startAngle, obj.angleDegrees)
        assertEquals(startAngularVelocity, obj.angularVelocity)
    }

    @Test
    fun `Setting mass updates mass data`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        obj.mass = 2000f

        assertEquals(100f, obj.radius, 0.01f)
        assertEquals(0.063f, obj.density, 0.01f)
        assertEquals(2000f, obj.mass, 0.01f)
    }

    @Test
    fun `Setting radius updates mass data`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        obj.radius = 10f

        assertEquals(10f, obj.radius, 0.01f)
        assertEquals(3.183f, obj.density, 0.01f)
        assertEquals(1000f, obj.mass, 0.01f)
    }

    @Test
    fun `Setting position updates value`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        val newPos = Vector2(10f, 11.1f)
        obj.position = newPos

        assertEquals(newPos, obj.position)
    }

    @Test
    fun `Setting velocity updates value`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        val newVel = Vector2(10f, 11.1f)
        obj.velocity = newVel

        assertEquals(newVel, obj.velocity)
    }

    @Test
    fun `Setting acceleration updates value`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        obj.setAcceleration(10f, 11.2f)

        assertEquals(10f, obj.acceleration.x)
        assertEquals(11.2f, obj.acceleration.y)
    }

    @Test
    fun `Updating acceleration value after doesn't update object's value`() {
        val acceleration = Vector2(-10f, 11.1f)
        val obj = WorldObject(
                name = "name1",
                gravityProfile = StrongerMostMassiveProfile(),
                body = body,
                shape = circle100,
                mass = 1000f,
                startAcceleration = acceleration
        )

        acceleration.x = 100f

        assertEquals(-10f, obj.acceleration.x)
        assertEquals(11.1f, obj.acceleration.y)
    }

    @Test
    fun `Setting angle updates value`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        obj.angleDegrees = 12f

        assertEquals(12f, obj.angleDegrees, 0.01f)
    }

    @Test
    fun `Setting angular velocity updates value`() {
        val obj = WorldObject(
                "name1",
                StrongerMostMassiveProfile(),
                body,
                circle100,
                1000f
        )

        obj.angularVelocity = 0.22f

        assertEquals(0.22f, obj.angularVelocity, 0.01f)
    }

    @Test
    fun `Update updates velocity for dynamic objects`() {
        val obj = WorldObject(
                name = "name1",
                gravityProfile = StrongerMostMassiveProfile(),
                body = body,
                shape = circle100,
                mass = 1000f,
                startVelocity = Vector2(1f, -1f),
                startAcceleration = Vector2(1f, -0.1f)
        )

        val timeDelta = 0.5f

        obj.update(timeDelta)

        assertEquals(1.5f, obj.velocity.x, 0.01f)
        assertEquals(-1.05f, obj.velocity.y, 0.01f)
    }

    @Test
    fun `Update doesn't update velocity for dynamic objects`() {
        val obj = WorldObject(
                name = "name1",
                gravityProfile = StrongerMostMassiveProfile(),
                body = staticBody,
                shape = circle100,
                mass = 1000f,
                startVelocity = Vector2(0f, 0f),
                startAcceleration = Vector2(1f, -0.1f)
        )

        val timeDelta = 0.5f

        obj.update(timeDelta)

        assertEquals(0f, obj.velocity.x, 0.01f)
        assertEquals(0f, obj.velocity.y, 0.01f)
    }
}