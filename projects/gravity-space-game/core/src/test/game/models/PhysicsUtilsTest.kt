package game.models

import org.junit.Assert.assertEquals
import org.junit.Test
import se.fkstudios.gravityspace.game.models.area
import se.fkstudios.gravityspace.game.models.density
import se.fkstudios.gravityspace.game.models.mass

class PhysicsUtilsTest {

    @Test
    fun `Calculate density from mass and radius`() {
        val mass = 1000f
        val radius = 5f

        val expectedDensity = 12.73f
        val actualDensity = density(mass, radius)

        assertEquals(expectedDensity, actualDensity, 0.01f)
    }

    @Test
    fun `Calculate mass from radius and density`() {
        val density = 14.25f
        val radius = 10f

        val expectedMass = 4476.76f
        val actualMass = mass(radius, density)

        assertEquals(expectedMass, actualMass, 0.01f)
    }

    @Test
    fun `Calculate area from mass and density`() {
        val density = 991.1f
        val mass = 71f

        val expectedArea = 0.07f
        val actualArea = area(mass, density)

        assertEquals(expectedArea, actualArea, 0.01f)
    }

    @Test
    fun `Calculate area from radius`() {
        val radius = 15.13f

        val expectedArea = 719.16f
        val actualArea = area(radius)

        assertEquals(expectedArea, actualArea, 0.01f)
    }
}