package se.fkstudios.gravityspace.game.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import se.fkstudios.gravityspace.game.controllers.GravitySpaceGame

class DesktopLauncher {
    companion object {
        @JvmStatic
        fun main(arg: Array<String>) {
            val config = LwjglApplicationConfiguration()
            config.width = 1920
            config.height = 1080
            config.fullscreen = false
            LwjglApplication(GravitySpaceGame(), config)
        }
    }
}
