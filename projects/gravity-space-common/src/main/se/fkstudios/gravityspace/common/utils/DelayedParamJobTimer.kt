package se.fkstudios.gravityspace.common.utils

class DelayedParamJobTimer<T> {

    var parameter: T? = null
        private set

    private var timeDelay: Float = 0f
    private var time: Float = 0f
    private var job: ((worldObject: T) -> Unit)? = null

    fun start(parameter: T, timeDelay: Float, job: (param: T) -> Unit) {
        this.parameter = parameter
        this.timeDelay = timeDelay
        this.time = 0f
        this.job = job
    }

    fun update(timeDelta: Float) {
        parameter?.let {
            time += timeDelta
            if (time > timeDelay) {
                job?.invoke(it)
                reset()
            }
        }
    }

    fun reset() {
        parameter = null
        timeDelay = 0f
        time = 0f
        job = null
    }
}