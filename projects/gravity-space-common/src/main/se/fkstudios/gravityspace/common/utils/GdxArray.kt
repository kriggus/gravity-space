package se.fkstudios.gravityspace.common.utils

import com.badlogic.gdx.utils.Array

typealias GdxArray<T> = Array<T>

fun <T> GdxArray<T>.has(index: Int): Boolean =
        index in 1..(size - 1)