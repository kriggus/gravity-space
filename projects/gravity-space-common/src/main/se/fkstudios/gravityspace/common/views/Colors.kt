package se.fkstudios.gravityspace.common.views

import com.badlogic.gdx.graphics.Color

val team1Color = Color.FOREST
val team2Color = Color.ORANGE

val iceBlue = Color(.55f, .9f, 1f, 0f)
val red = Color(.9f, .2f, .2f, 0f)
val darkGray = Color.DARK_GRAY
val gray = Color.GRAY
val lightGray = Color.LIGHT_GRAY
val clear = Color.CLEAR
val black = Color.BLACK