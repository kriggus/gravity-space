package se.fkstudios.gravityspace.common.utils

fun toUnsignedInt8(int8: Int): Int = when {
    int8 < -128 || int8 > 127 -> throw NumberFormatException("int8 out of range")
    else -> int8 and 0xFF
}

fun toSingedInt8(uInt8: Int): Int = when {
    uInt8 < 0 || uInt8 > 255 -> throw NumberFormatException("uInt8 out of range")
    uInt8 > 127 -> uInt8 - 256
    else -> uInt8
}