package se.fkstudios.gravityspace.common.network.messages

object MessageTypes {
    const val thrust: Int = 1
    const val angle: Int = 2
    const val fire: Int = 3
    const val selectTeam: Int = 4
    const val confirmTeam: Int = 5
    const val playerState: Int = 6
    const val playerName: Int = 7
    const val isAiming: Int = 8
}
