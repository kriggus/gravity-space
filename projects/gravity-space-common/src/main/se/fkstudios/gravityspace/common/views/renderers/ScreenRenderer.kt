package se.fkstudios.gravityspace.common.views.renderers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20

abstract class ScreenRenderer {

    protected fun clearScreen() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
    }

    protected fun clearScreen(color: Color) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a)
    }
}
