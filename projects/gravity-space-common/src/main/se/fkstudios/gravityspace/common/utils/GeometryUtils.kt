package se.fkstudios.gravityspace.common.utils

import com.badlogic.gdx.math.MathUtils

fun vectorLen(x: Int, y: Int): Float =
        Math.sqrt((x * x + y * y).toDouble()).toFloat()

fun vectorLen(x: Float, y: Float): Float =
        Math.sqrt((x * x + y * y).toDouble()).toFloat()

fun vectorNorX(x: Float, y: Float): Float {
    val length = vectorLen(x, y)
    return when (length) {
        0f -> 0f
        else -> x / length
    }
}

fun vectorNorY(x: Float, y: Float): Float {
    val length = vectorLen(x, y)
    return when (length) {
        0f -> 0f
        else -> y / length
    }
}

fun vectorRotateX(x: Float, y: Float, degrees: Float): Float {
    val radians = (degrees / MathUtils.radiansToDegrees)
    val cos = Math.cos(radians.toDouble()).toFloat()
    val sin = Math.sin(radians.toDouble()).toFloat()

    return x * cos - y * sin
}

fun vectorRotateY(x: Float, y: Float, degrees: Float): Float {
    val radians = (degrees / MathUtils.radiansToDegrees)
    val cos = Math.cos(radians.toDouble()).toFloat()
    val sin = Math.sin(radians.toDouble()).toFloat()

    return x * sin + y * cos
}

fun vectorAngleBetweenRad(x1: Float, y1: Float, x2: Float, y2: Float): Float =
        MathUtils.atan2(crsProduct(x1, y1, x2, y2), dotProduct(x1, y1, x2, y2))

fun vectorAngleBetweenDeg(x1: Float, y1: Float, x2: Float, y2: Float): Float =
        vectorAngleBetweenRad(x1, y1, x2, y2) * MathUtils.radiansToDegrees

fun crsProduct(x1: Float, y1: Float, x2: Float, y2: Float): Float =
        x1 * y2 - y1 * x2

fun dotProduct(x1: Float, y1: Float, x2: Float, y2: Float): Float =
        x1 * x2 + y1 * y2