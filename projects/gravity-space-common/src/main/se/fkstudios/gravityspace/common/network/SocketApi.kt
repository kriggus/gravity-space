package se.fkstudios.gravityspace.common.network

import com.badlogic.gdx.Net
import com.badlogic.gdx.net.ServerSocket
import com.badlogic.gdx.net.ServerSocketHints
import com.badlogic.gdx.net.Socket
import com.badlogic.gdx.net.SocketHints

class SocketApi(
        private val net: Net
) {

    fun newClientHints(): SocketHints =
            SocketHints()

    fun newClientSocket(): Socket =
            net.newClientSocket(Net.Protocol.TCP, SERVER_ADDRESS, PORT, newClientHints())

    fun newServerHints(): ServerSocketHints =
            ServerSocketHints()

    fun newServerSocket(): ServerSocket =
            net.newServerSocket(Net.Protocol.TCP, PORT, newServerHints())

    companion object {
        private const val SERVER_ADDRESS = "192.168.1.218"
        private const val PORT = 6499
    }
}