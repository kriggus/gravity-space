package se.fkstudios.gravityspace.common.network.messages

import se.fkstudios.gravityspace.common.utils.toSingedInt8
import se.fkstudios.gravityspace.common.utils.toUnsignedInt8
import java.io.IOException
import java.io.InputStream

class MessageParser(
        capacity: Int = 65536
) {
    private val buffer: ByteArray = ByteArray(capacity)
    private var bufferSize: Int = 0

    val type: Int?
        get() = when {
            bufferSize >= 2 -> toUnsignedInt8(buffer[0].toInt())
            else -> null
        }

    val payloadLength: Int?
        get() = when {
            bufferSize >= 2 -> toUnsignedInt8(buffer[1].toInt())
            else -> null
        }

    val hasCompleteMessage: Boolean
        get() {
            val payloadLength = payloadLength
            return type != null &&
                    payloadLength != null &&
                    bufferSize >= 2 &&
                    bufferSize >= (payloadLength + 2)
        }

    var stopped: Boolean = false
        private set

    fun nextPayload(payloadBuffer: ByteArray): Int {
        val payloadLength = payloadLength
        return when {
            hasCompleteMessage -> {
                for (i in 0 until payloadLength!!) {
                    payloadBuffer[i] = buffer[i + 2]
                }
                bufferSize -= (payloadLength + 2)
                buffer.forEachIndexed { index, byte ->
                    val newIndex = index - (payloadLength + 2)
                    if (newIndex > 0) {
                        buffer[newIndex] = byte
                    }
                }
                return payloadLength
            }
            else -> 0
        }
    }

    @Throws(IOException::class)
    fun parse(stream: InputStream) {
        while (!stopped && !hasCompleteMessage) {
            parseNextByte(stream)
        }
    }

    @Throws(IOException::class)
    private fun parseNextByte(stream: InputStream) {
        val uInt8 = stream.read()
        return when (uInt8) {
            -1 -> {
                bufferSize = 0
                stopped = true
            }
            else -> {
                val int8 = toSingedInt8(uInt8)
                val byte = int8.toByte()
                buffer[bufferSize++] = byte
            }
        }
    }
}