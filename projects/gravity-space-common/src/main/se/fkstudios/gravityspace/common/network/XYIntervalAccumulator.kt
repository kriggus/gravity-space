package se.fkstudios.gravityspace.common.network

class XYIntervalAccumulator(
        private val interval: Float = 0.1f
) {
    private var accumulatedTime: Float = 0f

    private var accumulatedX: Float = 0f
    private var accumulatedY: Float = 0f

    var count: Int = 0
        private set

    val x: Float
        get() = accumulatedX / count

    val y: Float
        get() = accumulatedY / count

    val hasIntervalPassed: Boolean
        get() = accumulatedTime >= interval

    fun addTime(timeDelta: Float) {
        accumulatedTime += timeDelta
    }

    fun addValues(x: Float, y: Float) {
        if (x == 0f && y == 0f) {
            accumulatedX = 0f
            accumulatedY = 0f
        } else {
            accumulatedX += x
            accumulatedY += y
        }
        count++
    }

    fun reset() {
        accumulatedTime = 0f
        accumulatedX = 0f
        accumulatedY = 0f
        count = 0
    }
}