package se.fkstudios.gravityspace.common.network.messages

import se.fkstudios.gravityspace.common.utils.toSingedInt8

fun newMessage(uByteType: Int, payload: ByteArray): ByteArray {
    val message = ByteArray(payload.size + 2)
    message[0] = toSingedInt8(uByteType).toByte()
    message[1] = toSingedInt8(payload.size).toByte()
    for (i in 0 until payload.size) {
        message[i + 2] = payload[i]
    }
    return message
}

fun newMessage(uByteType: Int): ByteArray {
    val message = ByteArray(2)
    message[0] = toSingedInt8(uByteType).toByte()
    message[1] = 0
    return message
}

fun ByteArray.toLogString(messageType: Int? = null, length: Int? = null): String {
    val size = length ?: size
    val builder = StringBuilder()
    builder.append("[")
    if (messageType != null) builder.append(messageType)
    for (i in 0 until size) {
        if (i != 0 || messageType != null) builder.append(", ")
        builder.append(this[i])
    }
    builder.append("]")
    return builder.toString()
}
