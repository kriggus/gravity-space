package se.fkstudios.gravityspace.common.utils

import com.badlogic.gdx.Screen

abstract class GdxScreen : Screen {

    override fun render(timeDelta: Float) {
        update(timeDelta)
        render()
    }

    abstract fun update(timeDelta: Float)

    abstract fun render()
}