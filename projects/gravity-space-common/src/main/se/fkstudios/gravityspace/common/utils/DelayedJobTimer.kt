package se.fkstudios.gravityspace.common.utils

class DelayedJobTimer {

    private var timeDelay: Float = 0f
    private var time: Float = 0f
    private var job: (() -> Unit)? = null

    fun start(timeDelay: Float, job: () -> Unit) {
        this.timeDelay = timeDelay
        this.time = 0f
        this.job = job
    }

    fun update(timeDelta: Float) {
        time += timeDelta
        if (time > timeDelay) {
            job?.invoke()
            reset()
        }
    }

    fun reset() {
        timeDelay = 0f
        time = 0f
        job = null
    }
}