package se.fkstudios.gravityspace.common.network

class IntervaledValue<T>(
        private val startValue: T,
        private val interval: Float = 0.1f
) {
    private var accumulatedTime: Float = 0f

    var value: T = startValue
        set(value) {
            field = value
            count++
        }

    var count: Int = 0
        private set

    val hasIntervalPassed: Boolean
        get() = accumulatedTime >= interval

    fun addTime(timeDelta: Float) {
        accumulatedTime += timeDelta
    }

    fun reset() {
        accumulatedTime = 0f
        value = startValue
        count = 0
    }
}