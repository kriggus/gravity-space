package se.fkstudios.gravityspace.common.utils

import com.badlogic.gdx.Graphics
import com.badlogic.gdx.physics.box2d.*

val Graphics.widthAsFloat: Float
    get() = width.toFloat()

val Graphics.heightAsFloat: Float
    get() = height.toFloat()

fun World.setSimpleContactListener(onContact: ((contact: Contact) -> Unit)?) {
    when (onContact) {
        null -> setContactListener(null)
        else -> setContactListener(object : ContactListener {
            override fun beginContact(contact: Contact?) = Unit
            override fun endContact(contact: Contact?) = Unit
            override fun preSolve(contact: Contact?, oldManifold: Manifold?) {
                if (contact != null) {
                    onContact(contact)
                }
            }
            override fun postSolve(contact: Contact?, impulse: ContactImpulse?) = Unit
        })
    }
}