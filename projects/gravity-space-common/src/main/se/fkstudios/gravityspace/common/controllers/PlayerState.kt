package se.fkstudios.gravityspace.common.controllers

enum class PlayerState(val raw: Byte) {
    LOBBY(0),
    PLAYING(1),
    PAUSED(2),
    WAITING(3);

    companion object {
        fun fromRaw(raw: Byte): PlayerState = when (raw) {
            LOBBY.raw -> LOBBY
            PLAYING.raw -> PLAYING
            PAUSED.raw -> PAUSED
            WAITING.raw -> WAITING
            else -> throw UnsupportedOperationException("Unknown raw type")
        }
    }
}