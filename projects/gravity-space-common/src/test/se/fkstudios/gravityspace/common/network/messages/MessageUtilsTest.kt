package se.fkstudios.gravityspace.common.network.messages

import org.junit.Test
import kotlin.test.assertEquals

class MessageUtilsTest {

    @Test
    fun `new message`() {
        val payload = ByteArray(2)
        payload[0] = 10
        payload[1] = 11

        val expected = ByteArray(4)
        expected[0] = 1
        expected[1] = 2
        expected[2] = 10
        expected[3] = 11

        val message = newMessage(1, payload)

        assertEquals(expected.toList(), message.toList())
    }

    @Test(expected = Exception::class)
    fun `error on too long message`() {
        val payload = ByteArray(257)
        for (i in 0 until 257) {
            payload[i] = 1
        }

        newMessage(1, payload)
    }
}
