package se.fkstudios.gravityspace.common.utils

import org.junit.Assert
import org.junit.Test
import java.lang.NumberFormatException

class NumberUtilsTest {

    @Test(expected = NumberFormatException::class)
    fun `Does throw error converting out of range negative to unsigned int8`() {
        toUnsignedInt8(-200)
    }

    @Test(expected = NumberFormatException::class)
    fun `Does throw error converting out of range positive to unsigned int8`() {
        toUnsignedInt8(200)
    }

    @Test
    fun `Does convert negative to unsigned int8`() {
        val result = toUnsignedInt8(-100)
        Assert.assertEquals(156, result)
    }

    @Test
    fun `Does convert positive to unsigned int8`() {
        val result = toUnsignedInt8(127)
        Assert.assertEquals(127, result)
    }

    @Test(expected = NumberFormatException::class)
    fun `Does throw error converting out of range negative to signed uInt8`() {
        toSingedInt8(-1)
    }

    @Test(expected = NumberFormatException::class)
    fun `Does throw error converting out of range positive to signed uInt8`() {
        toSingedInt8(256)
    }

    @Test
    fun `Does convert to negative signed uInt8`() {
        val result = toSingedInt8(165)
        Assert.assertEquals(-91, result)
    }

    @Test
    fun `Does convert to positive signed uInt8`() {
        val result = toSingedInt8(100)
        Assert.assertEquals(100, result)
    }
}
