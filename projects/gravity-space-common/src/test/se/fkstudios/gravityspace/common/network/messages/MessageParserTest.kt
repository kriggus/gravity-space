package se.fkstudios.gravityspace.common.network.messages

import com.nhaarman.mockito_kotlin.mock
import org.junit.Test
import org.mockito.Mockito.`when`
import java.io.InputStream
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class MessageParserTest {

    @Test
    fun `empty payload from nothing`() {
        val parser = MessageParser()
        val expectedPayload = ByteArray(256)

        val payload = ByteArray(256)
        val payloadLength = parser.nextPayload(payload)

        assertEquals(0, payloadLength)
        assertEquals(expectedPayload.toList(), payload.toList())
    }

    @Test
    fun `parse end of stream`() {
        val parser = MessageParser()
        val expectedPayload = ByteArray(256)
        expectedPayload[0] = 8
        expectedPayload[1] = 9

        val stream = mock<InputStream>()
        `when`(stream.read()).thenReturn(1, 2, 8, -1)

        parser.parse(stream)
        val parserPayloadLength = parser.payloadLength
        val parserType = parser.type
        val parserCompleteMsg = parser.hasCompleteMessage
        val parserStopped = parser.stopped

        assertNull(parserPayloadLength)
        assertNull(parserType)
        assertFalse(parserCompleteMsg)
        assertTrue(parserStopped)
    }

    @Test
    fun `parse and get payload from one messages`() {
        val parser = MessageParser()
        val expectedPayload = ByteArray(256)
        expectedPayload[0] = 8
        expectedPayload[1] = 9

        val stream = mock<InputStream>()
        `when`(stream.read()).thenReturn(1, 2, 8, 9)

        parser.parse(stream)
        val parserPayloadLength = parser.payloadLength
        val parserType = parser.type
        val parserHasCompleteMessage = parser.hasCompleteMessage
        val parserStopped = parser.stopped
        val payload = ByteArray(256)
        val parsedPayloadLength = parser.nextPayload(payload)
        val parserPayloadLengthAfter = parser.payloadLength
        val parserTypeAfter = parser.type
        val parserHasCompleteMessageAfter = parser.hasCompleteMessage
        val parserStoppedAfter = parser.stopped

        assertEquals(2, parserPayloadLength)
        assertEquals(1, parserType)
        assertTrue(parserHasCompleteMessage)
        assertFalse(parserStopped)
        assertEquals(2, parsedPayloadLength)
        assertEquals(expectedPayload.toList(), payload.toList())
        assertNull(parserPayloadLengthAfter)
        assertNull(parserTypeAfter)
        assertFalse(parserHasCompleteMessageAfter)
        assertFalse(parserStoppedAfter)
    }

    @Test
    fun `parse two and get payload from three messages`() {
        val parser = MessageParser()

        val expectedPayload1 = ByteArray(256)
        expectedPayload1[0] = 8
        expectedPayload1[1] = 9

        val expectedPayload2 = ByteArray(256)
        expectedPayload2[0] = 7

        val expectedPayload3 = ByteArray(256)

        val stream1 = mock<InputStream>()
        `when`(stream1.read()).thenReturn(1, 2, 8, 9)

        parser.parse(stream1)
        val parserPayload1Length = parser.payloadLength

        val payload1 = ByteArray(256)
        val parsedPayload1Length = parser.nextPayload(payload1)

        val stream2 = mock<InputStream>()
        `when`(stream2.read()).thenReturn(1, 1, 7)

        parser.parse(stream2)
        val parserPayload2Length = parser.payloadLength

        val payload2 = ByteArray(256)
        val parsedPayload2Length = parser.nextPayload(payload2)

        val payload3 = ByteArray(256)
        val parsedPayload3Length = parser.nextPayload(payload3)

        assertEquals(2, parserPayload1Length)
        assertEquals(2, parsedPayload1Length)
        assertEquals(expectedPayload1.toList(), payload1.toList())

        assertEquals(1, parserPayload2Length)
        assertEquals(1, parsedPayload2Length)
        assertEquals(expectedPayload2.toList(), payload2.toList())

        assertEquals(0, parsedPayload3Length)
        assertEquals(expectedPayload3.toList(), payload3.toList())
    }
}
