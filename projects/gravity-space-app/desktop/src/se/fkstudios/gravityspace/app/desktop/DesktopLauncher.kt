package se.fkstudios.gravityspace.app.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import se.fkstudios.gravityspace.app.controllers.GravitySpaceGame

class DesktopLauncher {
    companion object {
        @JvmStatic
        fun main(arg: Array<String>) {
            val config = LwjglApplicationConfiguration()
            config.width = 800
            config.height = 600
            LwjglApplication(GravitySpaceGame(), config)
        }
    }
}
