package se.fkstudios.gravityspace.app.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import se.fkstudios.gravityspace.common.views.darkGray
import se.fkstudios.gravityspace.common.views.lightGray
import se.fkstudios.gravityspace.common.views.renderers.ScreenRenderer

class CenteredTextScreenRenderer(
        private val screenWidth: Float,
        private val screenHeight: Float,
        private val spriteBatch: SpriteBatch,
        private val text: String,
        private val backgroundColor: Color = Color(.4f, .4f, .4f, 1.0f)
) : ScreenRenderer() {

    private val textFont: BitmapFont
    private val textGlyphLayout: GlyphLayout

    private val playerFont: BitmapFont

    init {
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontParameter()

        parameter.color = lightGray
        parameter.borderColor = darkGray
        parameter.borderWidth = screenFontBorderWith(4f)
        parameter.size = screenFontSize(32)

        textFont = generator.generateFont(parameter)
        textGlyphLayout = GlyphLayout(textFont, text)

        parameter.size = screenFontSize(24)
        playerFont = generator.generateFont(parameter)

        generator.dispose()
    }

    fun render(name: String?) {
        clearScreen(backgroundColor)

        spriteBatch.begin()

        var x = screenWidth / 2f - textGlyphLayout.width / 2f
        var y = screenHeight / 2f + textGlyphLayout.height / 2f
        textFont.draw(spriteBatch, text, x, y, 0f, -1, false)

        val noneNullName = name ?: ""
        x = screenWidth * .05f
        y = screenHeight - screenWidth * .05f
        playerFont.draw(spriteBatch, noneNullName, x, y, 0f, -1, false)

        spriteBatch.end()
    }
}
