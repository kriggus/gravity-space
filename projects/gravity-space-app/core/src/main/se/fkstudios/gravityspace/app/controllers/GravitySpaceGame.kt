package se.fkstudios.gravityspace.app.controllers

import com.badlogic.gdx.Game

class GravitySpaceGame : Game() {

    private var gameScreen: GameScreen? = null

    override fun create() {
        gameScreen = GameScreen()
        setScreen(gameScreen)
    }

    override fun dispose() {
        gameScreen?.dispose()
        super.dispose()
    }
}
