package se.fkstudios.gravityspace.app.controllers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import se.fkstudios.gravityspace.app.controllers.input.GameplayInputProcessor
import se.fkstudios.gravityspace.app.controllers.input.SelectTeamInputProcessor
import se.fkstudios.gravityspace.app.network.ClientApi
import se.fkstudios.gravityspace.app.network.NetworkManager
import se.fkstudios.gravityspace.app.views.GameScreenRenderer
import se.fkstudios.gravityspace.app.views.invertY
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.utils.DelayedJobTimer
import se.fkstudios.gravityspace.common.utils.GdxScreen
import se.fkstudios.gravityspace.common.utils.heightAsFloat
import se.fkstudios.gravityspace.common.utils.widthAsFloat

class GameScreen : GdxScreen() {

    private var screenStartX: Float = 0f
    private var screenStartY: Float = 0f
    private var screenEndX: Float = 0f
    private var screenEndY: Float = 0f
    private var clickAreaPressed: Boolean = false
    private var state: PlayerState = PlayerState.WAITING
    private var teamNo: Int? = null
    private var name: String = ""

    private val renderer: GameScreenRenderer = GameScreenRenderer()

    private val startClientJob = DelayedJobTimer()

    private val networkApi: ClientApi = ClientApi(Gdx.app, NetworkManager.client).apply {
        initCallbacks(
                onReceivedPlayerState = { state ->
                    this@GameScreen.state = state
                    inputProcessor = when (state) {
                        PlayerState.LOBBY -> selectTeamInputProcessor
                        PlayerState.PLAYING -> gameplayInputProcessor
                        PlayerState.WAITING -> null
                        PlayerState.PAUSED -> null
                    }
                    Gdx.input.inputProcessor = inputProcessor
                },
                onReceivedPlayerName = { name ->
                    this@GameScreen.name = name
                },
                onReceivedTeamNo = { teamNo ->
                    this@GameScreen.teamNo = teamNo
                },
                onStopped = {
                    startClientJob.start(5f, ::startClient)
                    this@GameScreen.name = ""
                    this@GameScreen.teamNo = null
                }
        )
    }

    private var inputProcessor: InputProcessor? = null

    private val selectTeamInputProcessor = SelectTeamInputProcessor(
            onClick = {
                if (state == PlayerState.LOBBY) {
                    when (teamNo) {
                        1 -> networkApi.sendSelectTeam(2)
                        2 -> networkApi.sendSelectTeam(1)
                    }
                }
            }
    )

    private val gameplayInputProcessor = GameplayInputProcessor(
            screenWidth = Gdx.graphics.widthAsFloat,
            screenHeight = Gdx.graphics.heightAsFloat,
            onDragChanged = { screenStartX: Float,
                              screenStartY: Float,
                              screenEndX: Float,
                              screenEndY: Float,
                              proportionX: Float,
                              proportionY: Float,
                              clickAreaPressed: Boolean ->
                if (state == PlayerState.PLAYING) {
                    this.screenStartX = screenStartX
                    this.screenStartY = screenStartY
                    this.screenEndX = screenEndX
                    this.screenEndY = screenEndY

                    when (clickAreaPressed) {
                        true -> networkApi.sendAngle(proportionX, -proportionY)
                        false -> networkApi.sendThrust(proportionX, -proportionY)
                    }
                }
            },
            onDragCanceled = { clickAreaPressed ->
                if (state == PlayerState.PLAYING) {
                    this.screenStartX = 0f
                    this.screenStartY = 0f
                    this.screenEndX = 0f
                    this.screenEndY = 0f

                    when (clickAreaPressed) {
                        true -> networkApi.sendFire()
                        false -> networkApi.sendThrust(0f, 0f)
                    }
                }
            },
            onClickAreaChanged = { clickAreaPressed ->
                if (state == PlayerState.PLAYING) {
                    this.clickAreaPressed = clickAreaPressed
                    networkApi.sendIsAiming(clickAreaPressed)
                }
            },
            onKeyPressed = { keycode ->
                if (state == PlayerState.PLAYING && keycode == Input.Keys.SPACE) {
                    networkApi.sendFire()
                }
            }
    )

    override fun show() {
        startClientJob.start(1f, networkApi::startClient)
        Gdx.input.inputProcessor = inputProcessor
    }

    override fun resume() {
        Gdx.input.inputProcessor = inputProcessor
    }

    override fun update(timeDelta: Float) {
        networkApi.update(timeDelta)
        startClientJob.update(timeDelta)
    }

    override fun render() {
        renderer.render(
                networkApi.isConnected,
                state,
                name,
                teamNo,
                screenStartX,
                screenStartY.invertY(),
                screenEndX,
                screenEndY.invertY(),
                clickAreaPressed
        )
    }

    override fun pause() {
        Gdx.input.inputProcessor = null
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        Gdx.input.inputProcessor = null
        startClientJob.reset()
        networkApi.stopClient()
        renderer.dispose()
    }

    override fun resize(width: Int, height: Int) = Unit
}