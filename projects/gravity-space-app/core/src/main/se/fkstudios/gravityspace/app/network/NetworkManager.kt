package se.fkstudios.gravityspace.app.network

import com.badlogic.gdx.Gdx
import se.fkstudios.gravityspace.common.network.SocketApi

object NetworkManager {
    private val socketApi = SocketApi(Gdx.net)
    val client = Client(socketApi)
}