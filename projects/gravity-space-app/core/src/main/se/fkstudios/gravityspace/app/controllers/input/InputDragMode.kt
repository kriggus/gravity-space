package se.fkstudios.gravityspace.app.controllers.input

enum class InputDragMode { DISCRETE, GRADUAL }