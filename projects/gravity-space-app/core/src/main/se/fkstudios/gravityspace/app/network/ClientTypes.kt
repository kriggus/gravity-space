package se.fkstudios.gravityspace.app.network

typealias ClientReceivedHandler = (messageType: Int, payloadBuffer: ByteArray, payloadLength: Int) -> Unit
typealias ClientStoppedHandler = () -> Unit