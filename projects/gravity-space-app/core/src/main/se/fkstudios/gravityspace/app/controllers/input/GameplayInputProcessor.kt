package se.fkstudios.gravityspace.app.controllers.input

import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.math.Vector2
import se.fkstudios.gravityspace.app.views.clickableScreenProportion
import se.fkstudios.gravityspace.app.views.dragableScreenProportion
import se.fkstudios.gravityspace.common.utils.vectorLen
import se.fkstudios.gravityspace.common.utils.vectorNorX
import se.fkstudios.gravityspace.common.utils.vectorNorY
import kotlin.math.roundToInt

class GameplayInputProcessor(
        private val screenWidth: Float,
        private val screenHeight: Float,
        private val onDragChanged: (
                screenStartX: Float,
                screenStartY: Float,
                screenEndX: Float,
                screenEndY: Float,
                proportionX: Float,
                proportionY: Float,
                clickAreaPressed: Boolean
        ) -> Unit,
        private val onDragCanceled: (
                clickAreaPressed: Boolean
        ) -> Unit,
        private val onClickAreaChanged: (
                clickAreaPressed: Boolean
        ) -> Unit,
        private val onKeyPressed: (
                keycode: Int
        ) -> Unit,
        private val dragMode: InputDragMode = InputDragMode.DISCRETE
) : InputProcessor {

    private var clickAreaPressed: Boolean = false
    private val dragPointerPositions: MutableMap<Int, Vector2> = mutableMapOf()
    private val dragLengthLowerBound: Int = 0
    private val dragLengthUpperBound: Int = (Math.min(screenWidth, screenHeight) / 3f).roundToInt()
    private val dragCenterPoint: Vector2 = Vector2(
            (screenWidth * clickableScreenProportion) + (screenWidth * dragableScreenProportion) / 2f,
            screenHeight / 2f
    )

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        when {
            isInClickArea(screenX.toFloat(), screenY.toFloat()) -> {
                clickAreaPressed = true
                onClickAreaChanged(clickAreaPressed)
            }
            isInDragArea(screenX.toFloat(), screenY.toFloat()) -> {
                dragPointerPositions[pointer] = Vector2(screenX.toFloat(), screenY.toFloat())
                when (dragPointerPositions.size) {
                    1 -> draggedOnePointer()
                    else -> onDragCanceled(clickAreaPressed)
                }
            }
        }
        return true
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        dragPointerPositions.remove(pointer)
        when {
            isInClickArea(screenX.toFloat(), screenY.toFloat()) -> {
                clickAreaPressed = false
                onClickAreaChanged(clickAreaPressed)
            }
            isInDragArea(screenX.toFloat(), screenY.toFloat()) -> {
                if (dragPointerPositions.size != 1) {
                    onDragCanceled(clickAreaPressed)
                }
            }
        }
        return true
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        updateDragPointerPosition(pointer, screenX, screenY)
        if (dragPointerPositions.size == 1) {
            draggedOnePointer()
        }
        return true
    }

    private fun updateDragPointerPosition(pointer: Int, screenX: Int, screenY: Int) {
        dragPointerPositions[pointer]?.x = screenX.toFloat()
        dragPointerPositions[pointer]?.y = screenY.toFloat()
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean =
            false

    override fun scrolled(amount: Int): Boolean =
            false

    override fun keyTyped(character: Char): Boolean =
            false

    override fun keyUp(keycode: Int): Boolean =
            false

    override fun keyDown(keycode: Int): Boolean {
        onKeyPressed(keycode)
        return true
    }

    private fun draggedOnePointer() {
        val pointerPosition = findDragPointerPositionByOrder(0) ?: return

        val draggedX = calcDraggedX(pointerPosition, dragCenterPoint)
        val draggedY = calcDraggedY(pointerPosition, dragCenterPoint)
        val draggedLength = vectorLen(draggedX, draggedY)

        when (draggedLength >= dragLengthLowerBound) {
            true -> draggedOnePointerOverLowerBound(dragCenterPoint, pointerPosition, draggedX, draggedY, draggedLength)
            false -> onDragCanceled(clickAreaPressed)
        }
    }

    private fun draggedOnePointerOverLowerBound(
            draggedStartPosition: Vector2,
            draggedEndPosition: Vector2,
            draggedX: Float,
            draggedY: Float,
            draggedLength: Float
    ) {
        val normalizedDraggedX = vectorNorX(draggedX, draggedY)
        val normalizedDraggedY = vectorNorY(draggedX, draggedY)

        val dragProportion = when (dragMode) {
            InputDragMode.GRADUAL -> Math.min(1f, draggedLength / dragLengthUpperBound)
            InputDragMode.DISCRETE -> 1f
        }

        val proportionX: Float = normalizedDraggedX * dragProportion
        val proportionY: Float = normalizedDraggedY * dragProportion

        onDragChanged(
                draggedStartPosition.x,
                draggedStartPosition.y,
                draggedEndPosition.x,
                draggedEndPosition.y,
                proportionX,
                proportionY,
                clickAreaPressed
        )
    }

    private fun isInClickArea(x: Float, y: Float): Boolean =
            x / screenWidth < clickableScreenProportion

    private fun isInDragArea(x: Float, y: Float): Boolean =
            x / screenWidth >= clickableScreenProportion

    private fun findDragPointerPositionByOrder(index: Int): Vector2? = dragPointerPositions
            .toList()
            .sortedBy { it.first }
            .getOrNull(index)
            ?.second

    private fun calcDraggedX(dragPointerPosition: Vector2, dragPointerStartPosition: Vector2): Float =
            dragPointerStartPosition.x - dragPointerPosition.x

    private fun calcDraggedY(dragPointerPosition: Vector2, dragPointerStartPosition: Vector2): Float =
            dragPointerStartPosition.y - dragPointerPosition.y

}