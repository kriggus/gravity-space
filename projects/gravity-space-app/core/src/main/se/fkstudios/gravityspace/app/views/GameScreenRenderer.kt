package se.fkstudios.gravityspace.app.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.utils.heightAsFloat
import se.fkstudios.gravityspace.common.utils.widthAsFloat

class GameScreenRenderer(
        screenWidth: Float = Gdx.graphics.widthAsFloat,
        screenHeight: Float = Gdx.graphics.heightAsFloat
) {
    private val shapeRenderer = ShapeRenderer()
    private val spriteBatch = SpriteBatch()
    private val pausedRenderer = CenteredTextScreenRenderer(screenWidth, screenHeight, spriteBatch, "PAUSED")
    private val waitingRenderer = CenteredTextScreenRenderer(screenWidth, screenHeight, spriteBatch, "WAITING FOR A NEW GAME...")
    private val connectingRenderer = CenteredTextScreenRenderer(screenWidth, screenHeight, spriteBatch, "CONNECTING...")
    private val selectTeamRenderer = SelectTeamScreenRenderer(screenWidth, screenHeight, spriteBatch)
    private val gameplayRenderer = GameplayScreenRenderer(screenWidth, screenHeight, shapeRenderer, spriteBatch)

    fun render(
            isConnected: Boolean,
            state: PlayerState,
            name: String?,
            teamNo: Int?,
            screenStartX: Float,
            screenStartY: Float,
            screenEndX: Float,
            screenEndY: Float,
            clickAreaPressed: Boolean
    ) {
        when {
            !isConnected -> connectingRenderer.render(name)
            state == PlayerState.LOBBY -> selectTeamRenderer.render(name, teamNo)
            state == PlayerState.PLAYING -> gameplayRenderer.render(name, screenStartX, screenStartY, screenEndX, screenEndY, clickAreaPressed)
            state == PlayerState.PAUSED -> pausedRenderer.render(name)
            state == PlayerState.WAITING -> waitingRenderer.render(name)
        }
    }

    fun dispose() {
        shapeRenderer.dispose()
        spriteBatch.dispose()
    }
}