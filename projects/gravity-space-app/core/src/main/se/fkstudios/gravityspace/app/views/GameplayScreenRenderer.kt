package se.fkstudios.gravityspace.app.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import se.fkstudios.gravityspace.common.views.darkGray
import se.fkstudios.gravityspace.common.views.renderers.ScreenRenderer

class GameplayScreenRenderer(
        private val screenWidth: Float,
        private val screenHeight: Float,
        private val shapeRenderer: ShapeRenderer,
        private val spriteBatch: SpriteBatch
) : ScreenRenderer() {

    private val fireForegroundColor = Color(1f, .4f, .4f, 1.0f)
    private val fireBackgroundColor = Color(.4f, 0f, 0f, 1.0f)

    private val thrustForegroundColor = Color(0f, .5f, 1f, 1.0f)
    private val thrustBackgroundColor = Color(0f, .2f, .4f, 1.0f)

    private val fireText = "FIRE"
    private val fireFont: BitmapFont
    private val fireGlyphLayout: GlyphLayout

    private val aimText = "AIM"
    private val thrustText = "THRUST"
    private val aimOrThrustFont: BitmapFont
    private val aimOrThrustLayout: GlyphLayout

    private val playerFont: BitmapFont

    init {
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.borderWidth = screenFontBorderWith(4f)
        parameter.size = screenFontSize(32)
        parameter.borderColor = darkGray

        fireFont = generator.generateFont(parameter)
        fireGlyphLayout = GlyphLayout(fireFont, fireText)

        aimOrThrustFont = generator.generateFont(parameter)
        aimOrThrustLayout = GlyphLayout(aimOrThrustFont, aimText)

        parameter.size = screenFontSize(24)
        playerFont = generator.generateFont(parameter)

        generator.dispose()
    }

    fun render(
            name: String?,
            screenStartX: Float,
            screenStartY: Float,
            screenEndX: Float,
            screenEndY: Float,
            clickAreaPressed: Boolean
    ) {
        val backgroundColor = selectBackgroundColor(clickAreaPressed)
        val foregroundColor = selectForegroundColor(clickAreaPressed)
        clearScreen(backgroundColor)
        renderTexts(clickAreaPressed, name, foregroundColor)
        renderLines(screenStartX, screenStartY, screenEndX, screenEndY, foregroundColor)
    }

    private fun renderTexts(clickAreaPressed: Boolean, name: String?, color: Color) {
        spriteBatch.begin()
        renderLeftText(color)
        renderRightText(selectRightText(clickAreaPressed), color)
        renderPlayerName(name, color)
        spriteBatch.end()
    }

    private fun renderRightText(text: String, color: Color) {
        aimOrThrustLayout.setText(aimOrThrustFont, text)
        val x = (screenWidth * clickableScreenProportion) + (screenWidth * dragableScreenProportion) / 2f - aimOrThrustLayout.width / 2f
        val y = screenHeight / 2f + fireGlyphLayout.height / 2f
        aimOrThrustFont.color = color
        aimOrThrustFont.draw(spriteBatch, text, x, y, 0f, -1, false)
    }

    private fun renderPlayerName(name: String?, color: Color) {
        val noneNullName = name ?: ""
        val x = screenWidth * .05f
        val y = screenHeight - screenWidth * .05f
        playerFont.color = color
        playerFont.draw(spriteBatch, noneNullName, x, y, 0f, -1, false)
    }

    private fun renderLines(
            screenStartX: Float,
            screenStartY: Float,
            screenEndX: Float,
            screenEndY: Float,
            foregroundColor: Color
    ) {
        Gdx.gl.glLineWidth(10f)
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)

        shapeRenderer.color = foregroundColor
        shapeRenderer.line(screenWidth * clickableScreenProportion, 0f, screenWidth * clickableScreenProportion, screenHeight)

        if (shouldRenderInputLine(screenStartX, screenStartY, screenEndX, screenEndY)) {
            shapeRenderer.color = foregroundColor
            shapeRenderer.line(screenStartX, screenStartY, screenEndX, screenEndY)
        }

        shapeRenderer.end()
        Gdx.gl.glLineWidth(1f)
    }

    private fun shouldRenderInputLine(screenStartX: Float, screenStartY: Float, screenEndX: Float, screenEndY: Float) =
            screenStartX != 0f || screenStartY != 0f || screenEndX != 0f || screenEndY != 0f

    private fun renderLeftText(color: Color) {
        val x = (screenWidth * clickableScreenProportion) / 2f - fireGlyphLayout.width / 2f
        val y = screenHeight / 2f + fireGlyphLayout.height / 2f
        fireFont.color = color
        fireFont.draw(spriteBatch, fireText, x, y, 0f, -1, false)
    }

    private fun selectRightText(clickAreaPressed: Boolean): String = when (clickAreaPressed) {
        true -> aimText
        false -> thrustText
    }

    private fun selectForegroundColor(clickAreaPressed: Boolean): Color = when (clickAreaPressed) {
        true -> fireForegroundColor
        false -> thrustForegroundColor
    }

    private fun selectBackgroundColor(clickAreaPressed: Boolean): Color = when (clickAreaPressed) {
        true -> fireBackgroundColor
        false -> thrustBackgroundColor
    }
}