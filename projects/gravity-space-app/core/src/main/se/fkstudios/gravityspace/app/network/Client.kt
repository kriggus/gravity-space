package se.fkstudios.gravityspace.app.network

import com.badlogic.gdx.net.Socket
import com.badlogic.gdx.utils.GdxRuntimeException
import se.fkstudios.gravityspace.common.network.SocketApi
import se.fkstudios.gravityspace.common.network.messages.MessageParser
import se.fkstudios.gravityspace.common.network.messages.toLogString
import java.io.IOException
import kotlin.concurrent.thread

class Client(
        private val socketApi: SocketApi
) {
    var running: Boolean = false
        private set

    private val lock: Any = Any()
    private var socket: Socket? = null
    private var parser: MessageParser? = null
    private var payloadBuffer = ByteArray(65536)

    private var onReceive: ClientReceivedHandler? = null
    private var onStopped: ClientStoppedHandler? = null

    fun start(
            onReceive: ClientReceivedHandler,
            onStopped: ClientStoppedHandler
    ) {
        synchronized(lock) {
            if (!running) {
                try {
                    running = true
                    this.onReceive = onReceive
                    this.onStopped = onStopped
                    parser = MessageParser()
                    socket = socketApi.newClientSocket()
                    startReceiveThread()
                } catch (e: GdxRuntimeException) {
                    System.out.println("Failed to start client. $e")
                    e.printStackTrace()
                    stopUnsafe()
                }
            }
        }
    }

    fun stop() {
        synchronized(lock) {
           stopUnsafe()
        }
    }

    private fun stopUnsafe() {
        if (running) {
            running = false
            socket?.dispose()
            socket = null
            parser = null
            onStopped?.invoke()
        }
    }

    fun send(message: ByteArray) {
        if (running) {
            try {
                System.out.println("Sending bytes to ${socket?.remoteAddress}. ${message.toLogString()}")
                socket?.outputStream?.write(message)
            } catch (e: IOException) {
                System.out.println("Error sending. $e")
                e.printStackTrace()
                stop()
            }
        }
    }

    private fun startReceiveThread() {
        thread {
            while (running) {
                try {
                    socket?.inputStream?.let { stream ->
                        parser!!.parse(stream)
                        when (parser!!.stopped) {
                            true -> stop()
                            else -> {
                                val messageType = parser!!.type!!
                                val payloadLength = parser!!.nextPayload(payloadBuffer)
                                System.out.println("Received bytes from ${socket?.remoteAddress} ${payloadBuffer.toLogString(messageType, payloadLength)}")
                                onReceive?.invoke(messageType, payloadBuffer, payloadLength)
                            }
                        }
                    }
                } catch (e: IOException) {
                    System.out.println("Client stopped with $e")
                    e.printStackTrace()
                    stop()
                }
            }
        }
    }
}