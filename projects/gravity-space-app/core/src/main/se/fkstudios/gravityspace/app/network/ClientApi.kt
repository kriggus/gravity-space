package se.fkstudios.gravityspace.app.network

import com.badlogic.gdx.Application
import se.fkstudios.gravityspace.common.controllers.PlayerState
import se.fkstudios.gravityspace.common.network.IntervaledValue
import se.fkstudios.gravityspace.common.network.XYIntervalAccumulator
import se.fkstudios.gravityspace.common.network.messages.MessageTypes
import se.fkstudios.gravityspace.common.network.messages.newMessage

class ClientApi(
        private val app: Application,
        private val client: Client,
        private val sendThrustAccumulator: XYIntervalAccumulator = XYIntervalAccumulator(.05f),
        private val sendAngleAccumulator: XYIntervalAccumulator = XYIntervalAccumulator(.05f),
        private val sendFireInterval: IntervaledValue<Boolean> = IntervaledValue(false, .5f)
) {

    val isConnected: Boolean
        get() = client.running

    private lateinit var onReceivedPlayerState: (state: PlayerState) -> Unit
    private lateinit var onReceivedTeamNo: (teamNo: Int) -> Unit
    private lateinit var onReceivedPlayerName: (playerName: String) -> Unit
    private lateinit var onStopped: () -> Unit

    private val handleReceivedMessage = { type: Int, payloadBuffer: ByteArray, payloadLength: Int ->
        when (type) {
            MessageTypes.playerState -> {
                val state = PlayerState.fromRaw(payloadBuffer[0])
                app.postRunnable {
                    onReceivedPlayerState(state)
                }
            }
            MessageTypes.confirmTeam -> {
                val teamNo = payloadBuffer[0].toInt()
                app.postRunnable {
                    onReceivedTeamNo(teamNo)
                }
            }
            MessageTypes.playerName -> {
                val name = String(payloadBuffer, 0, payloadLength)
                app.postRunnable {
                    onReceivedPlayerName(name)
                }
            }
        }
    }

    private val handleStopped: ClientStoppedHandler = {
        app.postRunnable {
            onStopped()
        }
    }

    fun initCallbacks(
            onReceivedPlayerState: (state: PlayerState) -> Unit,
            onReceivedTeamNo: (teamNo: Int) -> Unit,
            onReceivedPlayerName: (playerName: String) -> Unit,
            onStopped: () -> Unit
    ) {
        this.onReceivedPlayerState = onReceivedPlayerState
        this.onReceivedTeamNo = onReceivedTeamNo
        this.onReceivedPlayerName = onReceivedPlayerName
        this.onStopped = onStopped
    }

    fun update(timeDelta: Float) {
        sendThrustAccumulator.addTime(timeDelta)
        sendAccumulatedXYIfIntervalPassed(sendThrustAccumulator, MessageTypes.thrust)

        sendAngleAccumulator.addTime(timeDelta)
        sendAccumulatedXYIfIntervalPassed(sendAngleAccumulator, MessageTypes.angle)

        sendFireInterval.addTime(timeDelta)
        sendFireIfIntervalPassed()
    }

    fun sendThrust(x: Float, y: Float) {
        sendThrustAccumulator.addValues(x * 100f, y * 100f)
    }

    fun sendAngle(x: Float, y: Float) {
        sendAngleAccumulator.addValues(x * 100f, y * 100f)
        sendThrustAccumulator.reset()
    }

    fun sendFire() {
        sendFireInterval.value = true
    }

    fun sendSelectTeam(teamNo: Int) {
        val payload = ByteArray(1)
        payload[0] = teamNo.toByte()
        val message = newMessage(MessageTypes.selectTeam, payload)
        client.send(message)
    }

    fun sendIsAiming(isAiming: Boolean) {
        val payload = ByteArray(1)
        payload[0] = if (isAiming) 1 else 0
        val message = newMessage(MessageTypes.isAiming, payload)
        client.send(message)
    }

    fun startClient() {
        client.start(handleReceivedMessage, handleStopped)
    }

    fun stopClient() {
        client.stop()
    }

    private fun sendAccumulatedXYIfIntervalPassed(accumulator: XYIntervalAccumulator, uByteType: Int) {
        if (accumulator.hasIntervalPassed && accumulator.count > 0) {
            val payload = ByteArray(2)
            payload[0] = (accumulator.x).toByte()
            payload[1] = (accumulator.y).toByte()
            val message = newMessage(uByteType, payload)
            client.send(message)
            accumulator.reset()
        }
    }

    private fun sendFireIfIntervalPassed() {
        if (sendFireInterval.hasIntervalPassed && sendFireInterval.value) {
            client.send(newMessage(MessageTypes.fire))
            sendFireInterval.reset()
        }
    }
}