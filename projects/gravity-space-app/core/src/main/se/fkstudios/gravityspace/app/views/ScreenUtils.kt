package se.fkstudios.gravityspace.app.views

import com.badlogic.gdx.Gdx

const val clickableScreenProportion = .4f
const val dragableScreenProportion = 1f - clickableScreenProportion

fun Float.invertX(): Float =
        Gdx.graphics.width - this

fun Float.invertY(): Float =
        Gdx.graphics.height - this

fun screenFontSize(fontSize: Int): Int =
        Math.round(fontSize * Gdx.graphics.density)

fun screenFontBorderWith(width: Float): Float =
        width * Gdx.graphics.density
