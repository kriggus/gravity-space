package se.fkstudios.gravityspace.app.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import se.fkstudios.gravityspace.common.views.*
import se.fkstudios.gravityspace.common.views.renderers.ScreenRenderer

class SelectTeamScreenRenderer(
        private val screenWidth: Float,
        private val screenHeight: Float,
        private val spriteBatch: SpriteBatch
) : ScreenRenderer() {

    private val noTeamText = "NO TEAM! PLEASE WAIT..."
    private val noTeamFont: BitmapFont
    private val noTeamGlyphLayout: GlyphLayout

    private val inTeam1String = "IN TEAM $team1Name"
    private val inTeam2String = "IN TEAM $team2Name"
    private val inTeamFont: BitmapFont
    private val inTeamGlyphLayout: GlyphLayout

    private val joinTeam1String = "TAP TO JOIN TEAM $team1Name"
    private val joinTeam2String = "TAP TO JOIN TEAM $team2Name"
    private val joinTeamFont: BitmapFont
    private val joinTeamGlyphLayout: GlyphLayout

    private val playerFont: BitmapFont

    init {
        val generator = FreeTypeFontGenerator(Gdx.files.internal("roboto.ttf"))
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.borderWidth = screenFontBorderWith(4f)
        parameter.size = screenFontSize(32)
        parameter.borderColor = darkGray

        noTeamFont = generator.generateFont(parameter)
        noTeamGlyphLayout = GlyphLayout(noTeamFont, noTeamText)

        inTeamFont = generator.generateFont(parameter)
        inTeamGlyphLayout = GlyphLayout(inTeamFont, selectInTeamText(1))

        joinTeamFont = generator.generateFont(parameter)
        joinTeamGlyphLayout = GlyphLayout(joinTeamFont, selectJoinTeamText(1))

        parameter.size = screenFontSize(24)
        playerFont = generator.generateFont(parameter)

        generator.dispose()
    }

    fun render(name: String?, teamNo: Int?) {
        clearScreen(selectTeamColor(teamNo))
        renderTexts(name, teamNo)
    }

    private fun renderTexts(name: String?, teamNo: Int?) {
        spriteBatch.begin()
        when (teamNo) {
            1, 2 -> {
                renderInTeam(teamNo)
                renderJoinTeam(teamNo)
            }
            else -> renderNoTeam()
        }
        renderPlayerName(name, teamNo)
        spriteBatch.end()
    }

    private fun renderInTeam(teamNo: Int?) {
        val inTeamText = selectInTeamText(teamNo)
        inTeamGlyphLayout.setText(inTeamFont, inTeamText)
        val xIn = screenWidth / 2f - inTeamGlyphLayout.width / 2f
        val yIn = screenHeight * .6f + inTeamGlyphLayout.height / 2f
        inTeamFont.color = selectTeamColor(teamNo)
        inTeamFont.draw(spriteBatch, inTeamText, xIn, yIn, 0f, -1, false)
    }

    private fun renderJoinTeam(teamNo: Int?) {
        val joinTeamText = selectJoinTeamText(teamNo)
        joinTeamGlyphLayout.setText(joinTeamFont, joinTeamText)
        val xJoin = screenWidth / 2f - joinTeamGlyphLayout.width / 2f
        val yJoin = screenHeight * .4f + joinTeamGlyphLayout.height / 2f
        joinTeamFont.color = selectOtherTeamColor(teamNo)
        joinTeamFont.draw(spriteBatch, joinTeamText, xJoin, yJoin, 0f, -1, false)
    }

    private fun renderNoTeam() {
        val x = screenWidth / 2f - noTeamGlyphLayout.width / 2f
        val y = screenHeight / 2f + noTeamGlyphLayout.height / 2f
        noTeamFont.draw(spriteBatch, noTeamText, x, y, 0f, -1, false)
    }

    private fun renderPlayerName(name: String?, teamNo: Int?) {
        val noneNullName = name ?: ""
        val x = screenWidth * .05f
        val y = screenHeight - screenWidth * .05f
        playerFont.color = selectTeamColor(teamNo)
        playerFont.draw(spriteBatch, noneNullName, x, y, 0f, -1, false)
    }

    private fun selectInTeamText(teamNo: Int?): String = when (teamNo) {
        1 -> inTeam1String
        2 -> inTeam2String
        else -> ""
    }

    private fun selectJoinTeamText(teamNo: Int?): String = when (teamNo) {
        1 -> joinTeam2String
        2 -> joinTeam1String
        else -> ""
    }

    private fun selectTeamColor(teamNo: Int?): Color = when (teamNo) {
        1 -> team1Color
        2 -> team2Color
        else -> darkGray
    }

    private fun selectOtherTeamColor(teamNo: Int?): Color = when (teamNo) {
        1 -> team2Color
        2 -> team1Color
        else -> darkGray
    }
}